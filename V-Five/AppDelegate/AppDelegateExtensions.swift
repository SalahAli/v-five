//
//  AppDelegateExtensions.swift
//  Dieton
//
//  Created by Ahmed on 9/11/19.
//  Copyright © 2019 Ahmedsalahali@hotmail.com. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

extension AppDelegate {
    
    func configAppDelegate(){
        customNavBar()
        initWindow()
        checkAppLang()
        configGoogleMaps()
        ReachabilityChecker.shared.configureReachability()
        //Mark:- Delaiy Splash Screen Time
        RunLoop.current.run(until: Date(timeIntervalSinceNow: 5))
        //        //print all fonts
        //        for family in UIFont.familyNames.sorted() {
        //            let names = UIFont.fontNames(forFamilyName: family)
        //            print("Family: \(family) Font names: \(names)")
        //        }
    }
    
    func configGoogleMaps(){
        GMSServices.provideAPIKey(GoogleMapAPIKey)
        GMSPlacesClient.provideAPIKey(GoogleMapAPIKey)
    }
    
    func checkAppLang(){
        // Localization
        Lang.localize()
        if Language.currentLanguage == .arabic {
            AllUserDefaults.SETAPPLang("ar")
        } else {
            AllUserDefaults.SETAPPLang("en")
        }
    }
    
    func customNavBar() {
        // Mark:-
        // Navigationbar
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        
        UINavigationBar.appearance().barTintColor = .clear
        UINavigationBar.appearance().tintColor = UIColor(hexString: AppWhite) // Back Arrow Color
//        UINavigationBar.appearance().backIndicatorImage = UIImage(named: "backButton-shadow")
        
        let backButtonImage = UIImage(named: "back")
        UINavigationBar.appearance().backIndicatorImage = backButtonImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backButtonImage
        
        UINavigationBar.appearance().isTranslucent = true
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor(hexString: AppWhite)]
        
        //back button title
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: UIControl.State.highlighted)
        
        //        UITabBar.appearance().tintColor = UIColor.init(hexString: AppBlack)
        
    }
    
    func initWindow() {
        if Language.currentLanguage == .arabic {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        } else {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
//         setHomeAsRoot()
    }
    
    func setHomeAsRoot() {
        if AllUserDefaults.isLogin == true {
            let mainStoryboard = UIStoryboard(name: "HomePage" , bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "EnteryView") as! EnteryViewController
            let nav = UINavigationController(rootViewController: vc)
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = nav
            self.window?.makeKeyAndVisible()
        } else {
            let mainStoryboard = UIStoryboard(name: "Login" , bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let nav = UINavigationController(rootViewController: vc)
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = nav
            self.window?.makeKeyAndVisible()
        }
        
    }

    
}
