//
//  RequestManager.swift
//  EasyOrder
//
//  Created by Ahmed on 9/24/18.
//  Copyright © 2018 AhmedSalahAli@hotmail.com. All rights reserved.
//

import Foundation
//import SwiftyJSON
//import Alamofire

public enum HTTPMethod: String {
    case options = "OPTIONS"
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
    case trace   = "TRACE"
    case connect = "CONNECT"
}

class RequestManager {
    
    static let defaultManager = RequestManager()
    
    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    
    func WebServiceApiRequest<T: Codable>(service: Endpoints ,parameters: String?, complete: @escaping (T?,Bool)->()) {
        
        dataTask?.cancel()
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = TimeInterval(1000)
        configuration.timeoutIntervalForResource = TimeInterval(1000)
        
        let url = NSURL(string: service.url)
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: url! as URL)
        
        if let tocken = UserInfo.userToken()?.token {
            request.addValue("Bearer \(tocken)", forHTTPHeaderField: "Authorization")
        }
        
        request.httpMethod = service.httpMethod.rawValue
        if let postString = parameters {
            print("this is parameters:\(postString)")
            request.httpBody = postString.data(using: .utf8)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            debugPrint(request)
            guard let data = data, error == nil else {  // check for fundamental networking error
                print("error=\(String(describing: error))")
                complete(nil,true)
                
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse {
                print("server status code is: \(httpStatus.statusCode)")
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {  // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }
            print("data is: \(data)")
            if let json = try? JSONDecoder().decode(T.self, from: data) {
                dump("json ==============> \(json)")
                complete(json,false)
            }
            
            let response = NSString (data: data, encoding: String.Encoding.utf8.rawValue)
            print("response is \(response!)")
            
            complete(nil,true)
            
        })
        task.resume()
    }
}
