//
//  Utilities.swift
//  Beyoot
//
//  Created by Ahmed on 3/20/18.
//  Copyright © 2018 Ahmed . All rights reserved.
//

import Foundation
import UIKit

class Utilities {
    static  let defaults = UserDefaults.standard
    
    static let authID = "authID" // for firebase Authintication
    static let userTOKEN = "USERToken"
    static let userId = "userId"
    static let fcmToken = "fcmToken"
    static let isLogin = "isLogin"
    static let appLang = "appLang"
    
    
}
