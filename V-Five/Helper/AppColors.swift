//
//  AppColors.swift
//  MetrVocato
//
//  Created by Ahmed Salah on 6/19/18.
//  Copyright © 2018 Islam. All rights reserved.
//

import Foundation

let AppWhite: String = "#FFFFFF"
let AppYellow: String = "#FFBB3B"
let AppLightYellow: String = "#FFC04B"
let AppLightGray: String = "#D6DADC"
let AppBlueLine: String = "#8C99A7"
let TabBarBackground: String = "#FCFDFD"
let TabBarBorder: String = "#EAEBEC"
let shadowForBlueButton: String = "#30514E"
let dashedViewColor: String = "#C8C7CC"
let grayForPointTitle: String = "#ADBBC7"
let grayForPointLocation: String = "#6C7179"
let lightBlue: String = "#54769F"
let messageAreaBorder: String = "#212A37"
let dashedBorder: String = "#D0D6DB"
let blueGradientColor: String = "#152D4A"
let grayGradientColor: String = "#FCFCFC"
let verificationUnderLineColor: String = "#F4EEE7"

let darkBlue: String = "#09062e"
let green: String = "#348364"
let yellow: String = "#ffbb3b"


let orangeTagColor = "#F9A300"
let blueTagColor = "#0068AF"
let PurpleTagColor = "#8300FF"
let greenTagColor = "#578A00"
let redTagColor = "#8A0000"
let pinkTagColor = "#B200A8"
let turquoiseTagColor = "#00ADD2"

let darkBlack = "#333333"
let grayButton = "#DCDFE7"
let darkGreen = "#30514E"
let darkYellow = "#D4B256"



//App Mode
let appBackGroundLightMode = "#ECEFF3"
let appBackGroundDarkMode = "#2A2A2A" //Dark

let lightColorLightMode = "#FFFFFF"
let lightColorDarkMode = "#FFFFFF" //Dark

let redColorLightMode = "#EF3F3F"
let redColorDarkMode = "#EF3F3F" //Dark

let lightGrayButtonColorLightMode = "#F2F4F9"
let lightGrayButtonColorDarkMode = "#F2F4F9" //Dark

let grayButtonColorLightMode = "#DCDFDF"
let grayButtonColorDarkMode = "#DCDFDF" //Dark

let orangeColorLightMode = "#FFBB3B"
let orangeColorDarkMode = "#FFBB3B" //Dark

let defaultBlackColorLightMode = "#333333"
let defaultBlackColorDarkMode = "#333333" //Dark

let blueColorLightMode = "#1A2F50"
let blueColorDarkMode = "#1A2F50" //Dark

let darkGrayColorLightMode = "#ADADB4"
let darkGrayColorDarkMode = "#ADADB4" //Dark

let turkuazColorLightMode = "#0088EC"
let turkuazColorDarkMode = "#0088EC" //Dark

let whiteBackgroundColorLightMode = "#FFFFFF"
let darkBackgroundColorDarkMode = "#333333" //Dark

let seperatorViewColorLightMode = "#D9DBDF"
let seperatorViewColorDarkMode = "#3B3B3E"

let viewBorderColor = "DBDBDB"
