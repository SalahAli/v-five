//
//  ReachabilityChecker.swift
//  TerLive
//
//  Created by test on 11/04/2020.
//  Copyright © 2020 TerLive.com. All rights reserved.
//

import Foundation
import Reachability

final class ReachabilityChecker {
    private var reachability = Reachability()
    
    static var shared = ReachabilityChecker ()
    
    // MARK: - Reachability
    func configureReachability() {

        reachability?.whenReachable = { reachability in
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        }
        reachability?.whenUnreachable = { _ in
            print("Not reachable")
        }

        do {
            try reachability?.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reachabilityChanged),
                                               name: Notification.Name.reachabilityChanged,
                                               object: nil)
    }
    @objc func reachabilityChanged(notification: Notification) {
        if reachability!.connection == .wifi || reachability!.connection == .cellular {
            self.configureReachability()
        }
    }
    
    func isConnectedToNetwork() -> Bool {
        return reachability?.connection != Reachability.Connection.none
    }
}
