//
//  AppModeColor.swift
//  V-Five
//
//  Created by test on 27/06/2020.
//  Copyright © 2020 V-Five. All rights reserved.
//

import Foundation

class AppModeColor {
    
    static func getWhiteBackgroundColor() -> String {
        if UserInfo.currentAppMode() == "Light" {
            return whiteBackgroundColorLightMode
        }
        else {
            return darkBackgroundColorDarkMode
        }
    }
    
    static func getWhiteTextColor() -> String {
        if UserInfo.currentAppMode() == "Light" {
            return darkBackgroundColorDarkMode
        }
        else {
            return whiteBackgroundColorLightMode
        }
    }
    
    static func getSeperatorColor() -> String {
        if UserInfo.currentAppMode() == "Light" {
            return seperatorViewColorLightMode
        }
        else {
            return seperatorViewColorDarkMode
        }
    }
    
    static func getBackgroundColor() -> String {
        if UserInfo.currentAppMode() == "Light" {
            return appBackGroundLightMode
        }
        else {
            return appBackGroundDarkMode
        }
    }
    
    static func getLightColor() -> String {
        if UserInfo.currentAppMode() == "Light" {
            return lightColorLightMode
        }
        else {
            return lightColorDarkMode
        }
    }
    
    static func getRedColor() -> String {
        if UserInfo.currentAppMode() == "Light" {
            return redColorLightMode
        }
        else {
            return redColorDarkMode
        }
    }
    
    static func getLightGrayButtonColor() -> String {
        if UserInfo.currentAppMode() == "Light" {
            return lightGrayButtonColorLightMode
        }
        else {
            return lightGrayButtonColorDarkMode
        }
    }
    
    static func getGrayButtonColor() -> String {
        if UserInfo.currentAppMode() == "Light" {
            return grayButtonColorLightMode
        }
        else {
            return grayButtonColorDarkMode
        }
    }
    
    static func getOrangeColor() -> String {
        if UserInfo.currentAppMode() == "Light" {
            return orangeColorLightMode
        }
        else {
            return orangeColorDarkMode
        }
    }
    
    static func getDefaultBlackColor() -> String {
        if UserInfo.currentAppMode() == "Light" {
            return defaultBlackColorLightMode
        }
        else {
            return defaultBlackColorDarkMode
        }
    }
    
    static func getBlueColor() -> String {
        if UserInfo.currentAppMode() == "Light" {
            return blueColorLightMode
        }
        else {
            return blueColorDarkMode
        }
    }
    
    static func getDarkGrayColor() -> String {
        if UserInfo.currentAppMode() == "Light" {
            return darkGrayColorLightMode
        }
        else {
            return darkGrayColorDarkMode
        }
    }
    
    static func getTurkuazColor() -> String {
        if UserInfo.currentAppMode() == "Light" {
            return darkGrayColorLightMode
        }
        else {
            return darkGrayColorDarkMode
        }
    }
    
}
