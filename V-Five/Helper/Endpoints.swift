//
//  EndPoint.swift
//  TerLive
//
//  Created by test on 17/04/2020.
//  Copyright © 2020 TerLive.com. All rights reserved.
//

import Foundation

struct API {
    static let baseUrl = "https://terlive-events.herokuapp.com/"
}

public enum Endpoints {
    case login(apiVersion: Int)
    case verifyPhone(apiVersion: Int)
    case pickupLocation(apiVersion: Int)
    case availableTrips(apiVersion: Int)
    case completedTrips(apiVersion: Int)
    case userProfile(apiVersion: Int)
    case startTrip(apiVersion: Int)
    case endTrip(apiVersion: Int)
    case driverCurrentTrip(apiVersion: Int)
    case parentCurrentTrip(apiVersion: Int)
    case passengersList(apiVersion: Int, tripId: String)
    case checkIsPhoneExist(apiVersion: Int)
    case getAllTags(apiVersion: Int)
    case createNewTrip(apiVersion: Int)
    
    public var path: String {
        switch self {
            
        case .login(let apiVersion):
            return "api/user/en/v\(apiVersion)/login"
        case .verifyPhone(let apiVersion):
            guard let userType = UserInfo.currentUser()?.type else  { return ""}
            let api = "api/\(userType)/en/v\(apiVersion)/verifiedphone"
            return api
        case .pickupLocation(let apiVersion):
            guard let userType = UserInfo.currentUser()?.type else  { return ""}
            let api = "api/\(userType)/en/v\(apiVersion)/updatelocation"
            return api
        case .availableTrips(let apiVersion):
            guard let userType = UserInfo.currentUser()?.type else  { return ""}
            let api = "api/\(userType)/en/v\(apiVersion)/trips/available"
            return api
        case .completedTrips(let apiVersion):
            guard let userType = UserInfo.currentUser()?.type else  { return ""}
            let api = "api/\(userType)/en/v\(apiVersion)/trips/completed"
            return api
        case .userProfile(let apiVersion):
            guard let userType = UserInfo.currentUser()?.type else  { return ""}
            let api = "api/\(userType)/en/v\(apiVersion)/profile"
            return api
        case .startTrip(let apiVersion):
            guard let userType = UserInfo.currentUser()?.type else  { return ""}
            let api = "api/\(userType)/en/v\(apiVersion)/start/trip"
            return api
        case .endTrip(let apiVersion):
            guard let userType = UserInfo.currentUser()?.type else  { return ""}
            let api = "api/\(userType)/en/v\(apiVersion)/end/trip"
            return api
        case .driverCurrentTrip(let apiVersion):
            guard let userType = UserInfo.currentUser()?.type else  { return ""}
            let api = "api/\(userType)/en/v\(apiVersion)/today/trip"
            return api
        case .parentCurrentTrip(let apiVersion):
            guard let userType = UserInfo.currentUser()?.type else  { return ""}
            let api = "api/\(userType)/en/v\(apiVersion)/current/trip"
            return api
        case .passengersList(let apiVersion, let tripId):
            guard let userType = UserInfo.currentUser()?.type else  { return ""}
            let api = "api/\(userType)/en/v\(apiVersion)/passengers/\(tripId)"
            return api
        case .checkIsPhoneExist(let apiVersion):
            guard let userType = UserInfo.currentUser()?.type else  { return ""}
            let api = "api/\(userType)/en/v\(apiVersion)/check/phone"
            return api
        case .getAllTags(let apiVersion):
            let api = "api/trip/en/v\(apiVersion)/tags"
            return api
        case .createNewTrip(let apiVersion):
            let api = "api/trip/en/v\(apiVersion)/create"
            return api
        }
        
    }
    
    public var url: String {
        switch self {
        case .login:
            return "\(API.baseUrl)\(path)"
        case .verifyPhone:
            return "\(API.baseUrl)\(path)"
            
        case .pickupLocation:
            return "\(API.baseUrl)\(path)"
            
        case .availableTrips:
            return "\(API.baseUrl)\(path)"
        case .completedTrips:
            return "\(API.baseUrl)\(path)"
        case .userProfile:
            return "\(API.baseUrl)\(path)"
        case .startTrip:
            return "\(API.baseUrl)\(path)"
        case .endTrip:
            return "\(API.baseUrl)\(path)"
        case .driverCurrentTrip:
            return "\(API.baseUrl)\(path)"
        case .parentCurrentTrip:
            return "\(API.baseUrl)\(path)"
        case .passengersList:
            return "\(API.baseUrl)\(path)"
        case .checkIsPhoneExist:
            return "\(API.baseUrl)\(path)"
        case .getAllTags:
            return "\(API.baseUrl)\(path)"
        case .createNewTrip:
            return "\(API.baseUrl)\(path)"
        }
        
        
    }
    
    public var httpMethod: HTTPMethod {
        switch self {
        case .login:
            return .post
        case .verifyPhone:
            return .post
        case .pickupLocation:
            return .post
        case .availableTrips:
            return .get
        case .completedTrips:
            return .get
        case .userProfile:
            return .get
        case .startTrip:
            return .post
        case .endTrip:
            return .post
        case .driverCurrentTrip:
            return .get
        case .parentCurrentTrip:
            return .get
        case .passengersList:
            return .get
        case .checkIsPhoneExist:
            return .post
        case .getAllTags:
            return.get
        case .createNewTrip:
            return .post
        }
        
    }
}
