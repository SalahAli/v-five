//
//  AppModeIcons.swift
//  V-Five
//
//  Created by test on 11/07/2020.
//  Copyright © 2020 V-Five. All rights reserved.
//

import Foundation

class AppModeIcons {
    
    static func geteditProfileIcon() -> String {
        if UserInfo.currentAppMode() == "Light" {
            return "editIcon-LightMode"
        }
        else {
            return "editIcon-DarkMode"
        }
    }
    
    static func getbackProfileIcon() -> String {
        if UserInfo.currentAppMode() == "Light" {
            return "backArrow"
        }
        else {
            return "backArrow-DarkMode"
        }
    }
    
    static func getArrowProfileIcon() -> String {
        if UserInfo.currentAppMode() == "Light" {
            return "grayArrow"
        }
        else {
            return "profileArrow-DarkMode"
        }
    }
}
