//
//  UserDefaults.swift
//  MetrVocato
//
//  Created by Ahmed Salah on 5/31/18.
//  Copyright © 2018 Ahmed Salah. All rights reserved.
//

import Foundation

class AllUserDefaults {
    
    static func SETFCMTOKEN(_ fcm: String){
        Utilities.defaults.set(fcm, forKey: Utilities.fcmToken)
    }
    
    static func SETAPPLang(_ lang: String){
        Utilities.defaults.set(lang, forKey: Utilities.appLang)
    }
    
   
    static var isLogin: Bool { return Utilities.defaults.bool(forKey: Utilities.isLogin) }
    static var tokenFcm: String { return Utilities.defaults.string(forKey: Utilities.fcmToken) ?? "tt" }
    static var AppLang: String { return Utilities.defaults.string(forKey: Utilities.appLang) ?? "" }
    static var UserId: Int { return Utilities.defaults.integer(forKey: Utilities.userId) }
    static var userToken: String { return Utilities.defaults.string(forKey: Utilities.userTOKEN) ?? "" }


}





