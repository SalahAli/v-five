//
//  UserInfo.swift
//  TerLive
//
//  Created by test on 10/04/2020.
//  Copyright © 2020 TerLive.com. All rights reserved.
//

import Foundation

//enum AppMode: String {
//    case LightMode
//    case DarkMode
//}

struct UserDefaultsKeys {
    static let defaults = UserDefaults.standard
    
    static let country = "CurrentCountry"
    static let language = "CurrentLanguage"
    static let appleLanguage = "AppleLanguages"
    static let user = "User"
    static let portal = "Portal"
    static let userGrade = "userGrade"
    static let userToken = "userToken"
    static let isLogin = "isLogin"
    static let appMode = "AppMode"
}

class UserInfo {
    // MARK: - User
    static func currentUser() -> User? {
        if let object = UserDefaults.standard.object(forKey: UserDefaultsKeys.user) as? Data {
            if let user = try? JSONDecoder().decode(User.self, from: object) {
                return user
            }
        }
        return nil
    }
    
    static func setCurrentUser(user: User) {
        if let object = try? JSONEncoder().encode(user) {
            UserDefaults.standard.set(object, forKey: UserDefaultsKeys.user)
        }
    }
    
    static func userLoggedIn() -> Bool {
        return (UserInfo.currentUser()?.id != nil)
    }
    
    static func setUserToken(token: LoginResponse) {
        if let object = try? JSONEncoder().encode(token) {
            UserDefaults.standard.set(object, forKey: UserDefaultsKeys.userToken)
        }
    }
    
    static func userToken() -> LoginResponse? {
        if let object = UserDefaults.standard.object(forKey: UserDefaultsKeys.userToken) as? Data {
            if let token = try? JSONDecoder().decode(LoginResponse.self, from: object) {
                return token
            }
        }
        return nil
    }
    
    static func currentAppMode() -> String {
        if let mode = UserDefaults.standard.string(forKey: UserDefaultsKeys.appMode) {
            return mode
        } else {
            return "Light"
        }
    }
    
    static func setCurrentAppMode(mode: String) {
        UserDefaults.standard.set(mode, forKey: UserDefaultsKeys.appMode)
    }
    
    static var isLogin: Bool { return UserDefaultsKeys.defaults.bool(forKey: UserDefaultsKeys.isLogin ) }
}
