//
//  BackgroundCollectionViewCell.swift
//  V-Five
//
//  Created by test on 03/07/2020.
//  Copyright © 2020 V-Five. All rights reserved.
//

import UIKit

class BackgroundCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var backgroundImage: UIImageView!
}
