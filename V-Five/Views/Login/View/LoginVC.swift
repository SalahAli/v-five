//
//  LoginVC.swift
//  TerLive
//
//  Created by Ahmed on 2/22/20.
//  Copyright © 2020 TerLive.com. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

//    @IBOutlet weak var backgroundCollectionView: UICollectionView!
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var signInButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var gradientLayerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var signButton: UIButton!
//    var imagesArray: [String] = ["back","back1","back2"]
    var imagesArray: [String] = ["LoginBackground"]
    var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    var gameTimer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerKeyboardNotifications()
//        addGradientLayer()
        addGraidentColorTo(button: signButton)
        LoginPresenter.shared.loginDelegate = self
        LoginPresenter.shared.viewDidLoadIsFinished()
        self.signInButtonBottomConstraint.constant = -UIScreen.main.bounds.height / 3.7
        
        
//        timerAction()

        
        for index in 0..<imagesArray.count {
//            frame.origin.x = scrollView.frame.size.width * CGFloat(4)
//            frame.size = scrollView.frame.size
            
            let imgView = UIImageView(frame: frame)
            imgView.image = UIImage(named: imagesArray[index])
            imgView.frame = CGRect(x: 0, y: 0, width: scrollView.frame.size.width * CGFloat(5), height: scrollView.frame.size.height)
            self.scrollView.addSubview(imgView)
        }
        
        scrollView.contentSize = CGSize(width: (scrollView.frame.size.width * CGFloat(5)), height: scrollView.frame.size.height)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
            UIView.animate(withDuration: 10, animations: { () -> Void in
                self.scrollView.contentOffset.x = self.scrollView.frame.size.width * CGFloat(4)
                self.view.layoutIfNeeded()
            })
        }
        
        
    }
    
    var yOffset: CGFloat = 0

    func timerAction() {
        Timer.scheduledTimer(withTimeInterval: 3, repeats: true) { timer in
            self.yOffset += UIScreen.main.bounds.width
            DispatchQueue.main.async {
                UIView.animate(withDuration: 1.0) {
                    if self.scrollView.contentSize.width > self.yOffset {
                    self.scrollView.contentOffset.x = self.yOffset
                    } else {
//                        self.scrollView.contentOffset.x =
//                        timer.invalidate()
                        
//                        self.yOffset -= UIScreen.main.bounds.width
                        self.imagesArray.append(contentsOf: self.imagesArray)
                        for index in 0..<self.imagesArray.count {
                            self.frame.origin.x = self.scrollView.frame.size.width * CGFloat(index)
                            self.frame.size = self.scrollView.frame.size

                            let imgView = UIImageView(frame: self.frame)
                            imgView.image = UIImage(named: self.imagesArray[index])
                            self.scrollView.addSubview(imgView)
                        }

                        self.scrollView.contentSize = CGSize(width: (self.scrollView.frame.size.width * CGFloat(self.imagesArray.count)), height: self.scrollView.frame.size.height)
                        self.scrollView.contentOffset.x = self.yOffset
                        
//                        timer.invalidate()
                    }
                }
            }
        }
    }

    override func viewDidLayoutSubviews() {
       
    }

    func addGradientLayer() {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [UIColor.white.withAlphaComponent(0.8).cgColor,
                           UIColor.lightGray.withAlphaComponent(0.3).cgColor,
                           UIColor.black.withAlphaComponent(0.5).cgColor]
        gradient.locations = [0.0 ,0.4, 1.0]
        
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.gradientLayerView.frame.size.width, height: self.gradientLayerView.frame.size.height)
        gradient.cornerRadius = 0
        self.gradientLayerView.layer.insertSublayer(gradient, at: 0)
    }
    
    func addGraidentColorTo(button: UIButton) {
        let gradient: CAGradientLayer = CAGradientLayer()
//            gradient.startPoint = CGPoint(x: 1.0, y: 1.0)
//            gradient.endPoint = CGPoint(x: 0.0, y: 0.0)
            gradient.startPoint = CGPoint(x: 1.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.colors = [UIColor.init(hexString: darkBlue, alpha: 0.8).cgColor,
                           UIColor.init(hexString: darkGreen, alpha: 0.8).cgColor,
                               UIColor.init(hexString: green, alpha: 1.0).cgColor,
                               UIColor.init(hexString: green, alpha: 1.0).cgColor,
                               UIColor.init(hexString: yellow, alpha: 0.9 ).cgColor]
            gradient.locations = [0.0 ,0.25, 0.4, 0.6, 1.0]
            gradient.frame = CGRect(x: 0.0, y: 0.0, width: signButton.frame.size.width, height: signButton.frame.size.height)
            gradient.cornerRadius = 4.5
            button.layer.insertSublayer(gradient, at: 0)
            button.setTitleColor(UIColor.white, for: .normal)
        }
    
    @IBAction func registerTapped(_ sender: UIButton) {
        DispatchQueue.main.async {
//            self.loginIndicator.startAnimating()
//            self.registerBtn.setTitle("", for: .normal)
            LoginPresenter.shared.loginButtonPressed(phoneNumber: "\(self.phoneNumberTF.text!.replaceArToEn)", fcmToken: AllUserDefaults.tokenFcm)
        }
    }

    private func registerKeyboardNotifications() {
        hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    // MARK: - Keyboard scrolling handling
    @objc func keyboardWillShow(notification: Notification) {
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            UIView.animate(withDuration: 0.3, animations: {
                self.signInButtonBottomConstraint.constant = -keyboardHeight - UIScreen.main.bounds.height / 10 //70.0
                self.view.layoutIfNeeded()
            })
        }
        
    }
    
    @objc func keyboardWillBeHidden(notification: Notification) {
        UIView.animate(withDuration: 0.3, animations: {
            self.signInButtonBottomConstraint.constant = -UIScreen.main.bounds.height / 3.7
            self.view.layoutIfNeeded()
        })
    }
    
    func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }

}

extension LoginVC: onLoginPresenterToViewProtocol {
    
    func navigateToVerificationCodeScreen() {
        DispatchQueue.main.async {
//            self.loginIndicator.stopAnimating()
//            self.registerBtn.setTitle("Verify", for: .normal)
            let storyBoard = UIStoryboard(name: "PhoneVerification", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "PhoneVerificationVC") as! PhoneVerificationVC
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func onFailuarLogin(errorMessage: String) {
        DispatchQueue.main.async {
//            self.loginIndicator.stopAnimating()
//            self.registerBtn.setTitle("Verify", for: .normal)
            self.alert(message: errorMessage)
        }
    }
    
    func showConnectionErro() {
        DispatchQueue.main.async {
//            self.loginIndicator.stopAnimating()
//            self.registerBtn.setTitle("Verify", for: .normal)
        }
    }
}

//
//extension LoginVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 3
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BackgroundCell", for: indexPath) as? BackgroundCollectionViewCell {
//            cell.backgroundImage.image = UIImage(named: imagesArray[indexPath.row])
//            return cell
//        }
//        return UICollectionViewCell()
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//
//        return 10
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        let frameSize = collectionView.frame.size
//        return CGSize(width: 400, height: 400)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//
//        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
//    }
//
////    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
////    }
////
////    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
////        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
////    }
////
////    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
////        return 0
////    }
////
////    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
////        return 0
////    }
////
////    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
////        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
////    }
////
////    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
////            return CGSize(width: collectionView.frame.width, height: 0)
////    }
//}
