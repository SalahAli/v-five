//
//  LoginResponse.swift
//  TerLive
//
//  Created by test on 09/04/2020.
//  Copyright © 2020 TerLive.com. All rights reserved.
//

import Foundation


struct LoginResponse : Codable {
    let data : User?
    let exist : Bool?
    let message : String?
    let status : Int?
    let token : String?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case exist = "exist"
        case message = "message"
        case status = "status"
        case token = "token"
        
    }
    
}

struct User : Codable {
    let id : String?
    let blocked : Bool?
    let fcmToken : String?
    let line : String?
    let phoneNumber : String?
    let phoneVerified : Bool?
    let portal : String?
    let profileImage : String?
    let username : String?
    let type : String = "user"
    let longitude: String?
    let latitude: String?
    let isChangeLocation: Bool?
    let associationId: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case blocked = "blocked"
        case fcmToken = "fcmToken"
        case line = "line"
        case phoneNumber = "phoneValue"
        case phoneVerified = "phoneVerified"
        case portal = "portal"
        case profileImage = "profileImage"
        case username = "username"
        case type = "type"
        case longitude = "longitude"
        case latitude = "latitude"
        case isChangeLocation = "changeLocation"
        case associationId = "associationId"
    }
    
}

