//
//  LoginPresenter.swift
//  TerLive
//
//  Created by test on 09/04/2020.
//  Copyright © 2020 TerLive.com. All rights reserved.
//

import Foundation

class LoginPresenter: onLoginModelToPresenterProtocol {
    static let shared = LoginPresenter()
    var loginDelegate:onLoginPresenterToViewProtocol!
    
    
    func onConnectionError() {
        loginDelegate.showConnectionErro()
    }
    
    func viewDidLoadIsFinished() {
        LoginModel.shared.loginDelegate = self
    }
    
    func loginButtonPressed(phoneNumber: String, fcmToken: String) {
        LoginModel.shared.login(phoneNumber: phoneNumber, fcmToken: fcmToken)
    }
    
    func onSuccessLogin(response: User) {
        let userInfo = response
        UserInfo.setCurrentUser(user: userInfo)
        loginDelegate.navigateToVerificationCodeScreen()
    }
    
    func onFailuarLogin(errorMessage: String) {
        loginDelegate.onFailuarLogin(errorMessage: errorMessage)
    }
    
}

protocol onLoginPresenterToViewProtocol{
    func navigateToVerificationCodeScreen()
    func onFailuarLogin(errorMessage:String)
    func showConnectionErro()
}

