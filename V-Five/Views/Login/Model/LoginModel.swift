//
//  LoginModel.swift
//  TerLive
//
//  Created by test on 10/04/2020.
//  Copyright © 2020 TerLive.com. All rights reserved.
//

import Foundation

class LoginModel {
    
    static let shared = LoginModel()
    var loginDelegate:onLoginModelToPresenterProtocol!
    
    func login(phoneNumber: String, fcmToken: String) {
        let param: [String:Any] = ["phone": phoneNumber, "fcmToken": fcmToken]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: param, options: [])
        guard let jsonString = String(data: jsonData!, encoding: .utf8) else { return }
        
        RequestManager.defaultManager.WebServiceApiRequest(service: Endpoints.login(apiVersion: 1), parameters: jsonString) { (respone: LoginResponse?, error: Bool) in
            if !error {
                if respone?.status == 200 {
                    if let user = respone?.data {
                        if let token = respone {
                            UserInfo.setUserToken(token: token)
                        }
                        self.loginDelegate.onSuccessLogin(response: user)
                    }
                } else {
                    self.loginDelegate?.onFailuarLogin(errorMessage: "User doesn't exist")
                }
                
            } else {
                if !ReachabilityChecker.shared.isConnectedToNetwork() {
                    self.loginDelegate.onConnectionError()
                }
            }
        }
    }
}

protocol onLoginModelToPresenterProtocol{
    func onSuccessLogin(response: User)
    func onFailuarLogin(errorMessage:String)
    func onConnectionError()
}

