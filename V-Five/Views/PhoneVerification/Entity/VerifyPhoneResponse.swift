//
//  VerifyPhoneResponse.swift
//  TerLive
//
//  Created by Ahmed on 4/15/20.
//  Copyright © 2020 TerLive.com. All rights reserved.
//

import Foundation

struct VerifyPhoneResponse: Codable {
    let message : String?
    let status : Int?
}
