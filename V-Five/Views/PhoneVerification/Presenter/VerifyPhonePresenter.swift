//
//  VerifyPhonePresenter.swift
//  TerLive
//
//  Created by Ahmed on 4/16/20.
//  Copyright © 2020 TerLive.com. All rights reserved.
//

import Foundation

class VerifyPhonePresenter: onVerifyPhoneModelToPresenterProtocol {
   
    static let shared = VerifyPhonePresenter()
    var verifyPhoneDelegate: onVerifyPhonePresenterToViewProtocol!
    
    func onSuccessVerifyPhone(response: VerifyPhoneResponse) {
        verifyPhoneDelegate.navigateToHomeScreen(res: response)
    }
    
    func onFailuarVerifyPhone(errorMessage: String) {
        verifyPhoneDelegate.onFailuarVerifyPhone(errorMessage: errorMessage)
    }
    
    func onConnectionError() {
        verifyPhoneDelegate.showConnectionErro()
    }
    
    func viewDidLoadIsFinished() {
        VerifyPhoneModel.shared.verifyPhoneDelegate = self
    }
    
    func verifyPhoneButtonPressed() {
        VerifyPhoneModel.shared.verifyPhoneAPI()
    }

    
}

protocol onVerifyPhonePresenterToViewProtocol{
    func navigateToHomeScreen(res: VerifyPhoneResponse)
    func onFailuarVerifyPhone(errorMessage:String)
    func showConnectionErro()
}
