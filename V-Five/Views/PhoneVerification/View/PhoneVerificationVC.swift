//
//  VerificationPhoneVC.swift
//  TerLive
//
//  Created by Ahmed on 2/23/20.
//  Copyright © 2020 TerLive.com. All rights reserved.
//

import UIKit
import FirebaseAuth

class PhoneVerificationVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var verificationContainerView: UIView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var VerificationViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var codeNo1Txt: UITextField!
    @IBOutlet weak var codeNo2Txt: UITextField!
    @IBOutlet weak var codeNo3Txt: UITextField!
//    @IBOutlet weak var codeNo4Txt: UITextField!
    @IBOutlet weak var containerStackView: UIStackView!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var resendCodeBtn: UIButton!
    
    var parameters: NSMutableDictionary = [:]
    var verificationCode:String!
    var isFromLogin = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        registerKeyboardNotifications()
        addGradientLayer()
        phoneLabel.text = UserInfo.currentUser()?.phoneNumber
        VerifyPhonePresenter.shared.verifyPhoneDelegate = self
        VerifyPhonePresenter.shared.viewDidLoadIsFinished()

        //Mark: send phone number to firebase
//        getCodeFromFirebase()
        
        //uncoment this line
        VerifyPhonePresenter.shared.verifyPhoneButtonPressed()
    }
    
    override func viewDidLayoutSubviews() {
        verificationContainerView.addShadowForView(shadowOpicty: 1.0,
                                                   corner: 20.0,
                                                   shadowOffsetWidth: 0,
                                                   shadowOffsetHeight: -10,
                                                   color: UIColor.white.withAlphaComponent(0.8))
    }
    
    func addGradientLayer() {
        let gradient: CAGradientLayer = CAGradientLayer()
        
        gradient.colors = [UIColor.init(hexString: grayGradientColor, alpha: 1.0).cgColor,
                           UIColor.init(hexString: grayGradientColor, alpha: 0.75).cgColor,
                           UIColor.init(hexString: blueGradientColor, alpha: 0.22 ).cgColor]
        gradient.locations = [0.0 ,0.4, 1.0]
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.gradientView.frame.size.width, height: 420.0)
        self.gradientView.layer.insertSublayer(gradient, at: 0)
    }
    
    @IBAction func resendButtonPressed(_ sender: Any) {
         getCodeFromFirebase()
    }
    
    
    func setupView(){
        //        containerView.semanticContentAttribute = .forceLeftToRight
        containerStackView.semanticContentAttribute = .forceLeftToRight
        codeNo1Txt.addTarget(self, action: #selector(self.textFieldDidChanged(textField:)), for: .editingChanged)
        codeNo2Txt.addTarget(self, action: #selector(self.textFieldDidChanged(textField:)), for: .editingChanged)
        codeNo3Txt.addTarget(self, action: #selector(self.textFieldDidChanged(textField:)), for: .editingChanged)
//        codeNo4Txt.addTarget(self, action: #selector(self.textFieldDidChanged(textField:)), for: .editingChanged)
        
        [codeNo1Txt,
         codeNo2Txt,
         codeNo3Txt].forEach{
            $0?.bottomBoarder(color: UIColor.init(hexString: verificationUnderLineColor, alpha: 1.0).cgColor, height: 5)
            $0?.delegate = self
            $0?.attributedPlaceholder = NSAttributedString(string: "•", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(hexString: verificationUnderLineColor, alpha: 1.0)])
        }
        
        resendCodeBtn.bottomBoarder(color: UIColor.init(hexString: AppLightYellow, alpha: 1).cgColor, height: 1)
        phoneLabel.bottomBoarder(color: UIColor.init(hexString: AppBlueLine, alpha: 1).cgColor, height: 1)
        
    }
    
    
    
    @objc func textFieldDidChanged(textField: UITextField) {
        var text = textField.text
        if (text?.utf16.count)! < 2  {
            switch textField {
            case codeNo1Txt:
                codeNo1Txt.bottomBoarder(color: UIColor(red: 255/255, green: 183/255, blue: 30/255, alpha: 1.0).cgColor, height: 5)
                codeNo2Txt.resignFirstResponder()
                
            case codeNo2Txt:
                codeNo2Txt.bottomBoarder(color: UIColor(red: 255/255, green: 183/255, blue: 30/255, alpha: 1.0).cgColor, height: 5)
                codeNo3Txt.resignFirstResponder()
                
            case codeNo3Txt:
                codeNo3Txt.bottomBoarder(color: UIColor(red: 255/255, green: 183/255, blue: 30/255, alpha: 1.0).cgColor, height: 5)
                codeNo3Txt.becomeFirstResponder()
                
//            case codeNo4Txt:
//                codeNo4Txt.becomeFirstResponder()
                
            default:
                break
            }
        }  else if (text?.utf16.count)! == 2  {
            switch textField {
            case codeNo1Txt:
                codeNo1Txt.bottomBoarder(color: UIColor(red: 255/255, green: 183/255, blue: 30/255, alpha: 1.0).cgColor, height: 5)
//                codeNo4Txt.resignFirstResponder()
                codeNo2Txt.becomeFirstResponder()
            case codeNo2Txt:
                codeNo2Txt.bottomBoarder(color: UIColor(red: 255/255, green: 183/255, blue: 30/255, alpha: 1.0).cgColor, height: 5)
//                codeNo4Txt.resignFirstResponder()
                codeNo3Txt.becomeFirstResponder()
            case codeNo3Txt:
                codeNo3Txt.bottomBoarder(color: UIColor(red: 255/255, green: 183/255, blue: 30/255, alpha: 1.0).cgColor, height: 5)
//                codeNo4Txt.resignFirstResponder()
                codeNo3Txt.resignFirstResponder()
//            case codeNo4Txt:
//                codeNo4Txt.resignFirstResponder()
            default:
                break
            }
        }
        else {
            textField.text = ""
        }
    }
    
    func navigateToHomeScreen() {
        DispatchQueue.main.async {
//            let storyBoardName: UIStoryboard = UIStoryboard(name: "TabBarStoryBoard", bundle: nil)
//            if let tabBar = storyBoardName.instantiateViewController(withIdentifier: "CustomTabBarView") as? CustomTabBar {
//                tabBar.selectedViewController = tabBar.viewControllers?[0]
//                self.present(tabBar, animated: false, completion: nil)
//            }
//
//            let mainStoryboard = UIStoryboard(name: "HomePage" , bundle: nil)
//            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ParentTabBarView") as! ParentTabBarViewController
//            self.present(vc, animated: false, completion: nil)
            
            
//            let mainStoryboard = UIStoryboard(name: "HomePage" , bundle: nil)
//            let destination = mainStoryboard.instantiateViewController(withIdentifier: "ParentTabBarView") as! ParentTabBarViewController
////            let navigationController = UINavigationController(rootViewController: destination)
//            let appdelegate = UIApplication.shared.delegate as! AppDelegate
//            appdelegate.window!.rootViewController = destination
            
            
            let story = UIStoryboard(name: "HomePage", bundle:nil)
            let viewController = story.instantiateViewController(withIdentifier: "ParentTabBarView") as! ParentTabBarViewController
            let navigationController = UINavigationController(rootViewController: viewController)
            UIApplication.shared.windows.first?.rootViewController = navigationController
            UIApplication.shared.windows.first?.makeKeyAndVisible()
            
            
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == codeNo3Txt {
            if let code = codeNo3Txt.text, code.count == 2 {
                verifyPhoneByFirebase()
            }
            
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        //        sharedPhone = nil
    }
    
    @IBAction func dismissTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    private func registerKeyboardNotifications() {
        hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    // MARK: - Keyboard scrolling handling
    @objc func keyboardWillShow(notification: Notification) {
        UIView.animate(withDuration: 1.0, animations: {
            self.VerificationViewBottomConstraint.constant = 50.0
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func keyboardWillBeHidden(notification: Notification) {
        UIView.animate(withDuration: 1.0, animations: {
            self.VerificationViewBottomConstraint.constant = 0.0
            self.view.layoutIfNeeded()
        })
    }
    
    func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    
}


extension PhoneVerificationVC {
    func getCodeFromFirebase(){
        guard let fullPhoneNum = UserInfo.currentUser()?.phoneNumber else { return }
        // Send  Request Verification code to Firebase Auth
        
        PhoneAuthProvider.provider().verifyPhoneNumber("\(fullPhoneNum.replaceArToEn)", uiDelegate: nil) {(verificationID, error) in
            if let error = error {
                debugPrint(error)
                debugPrint("firebase phone error")
                
            } else {
                //                self.stopIndicator()
                debugPrint(verificationID!)
                debugPrint("firebase successfully done")
                // Save Verification code from Firebase to userdefault
                Utilities.defaults.set(verificationID!, forKey: Utilities.authID)
            }
        }
        
    }
    
    func verifyPhoneByFirebase(){
        //start code
        self.verificationCode = codeNo1Txt.text! + codeNo2Txt.text! + codeNo3Txt.text!
        if let verifiedCode =  self.verificationCode {
            let credential: PhoneAuthCredential = PhoneAuthProvider.provider().credential(withVerificationID: Utilities.defaults.string(forKey: Utilities.authID)!, verificationCode: verifiedCode.replaceArToEn)
            Auth.auth().signIn(with: credential, completion: { (user, error) in
                if error != nil {
                    self.alert(message: NSLocalizedString("Wrong code", comment: ""))
                } else {
                    debugPrint("success verification code fron firebase method")
//                    debugPrint("phone number: \(String(describing: user?.phoneNumber))")
//                    let userInfo = user?.providerData[0]
//                    debugPrint("provider ID: \(String(describing: userInfo?.providerID))")
                    //                    self.verifyPhoneAtBackEnd()
                    //                    self.loginOrRegisterUser()
                    VerifyPhonePresenter.shared.verifyPhoneButtonPressed()
                }
            })
            
        }
        // End code
    }
    
    
}

extension PhoneVerificationVC: onVerifyPhonePresenterToViewProtocol {
    func navigateToHomeScreen(res: VerifyPhoneResponse) {
        if let status = res.status {
            if status == 200 {
                UserDefaultsKeys.defaults.set(true, forKey: UserDefaultsKeys.isLogin)
                self.navigateToHomeScreen()
            }
        }
    }
    
    func onFailuarVerifyPhone(errorMessage: String) {
        
    }
    
    func showConnectionErro() {
        
    }
    
    
}
