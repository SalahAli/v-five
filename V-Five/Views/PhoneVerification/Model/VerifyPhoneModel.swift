//
//  VerifyPhoneModel.swift
//  TerLive
//
//  Created by Ahmed on 4/15/20.
//  Copyright © 2020 TerLive.com. All rights reserved.
//

import Foundation

class VerifyPhoneModel {
    static let shared = VerifyPhoneModel()
    var verifyPhoneDelegate: onVerifyPhoneModelToPresenterProtocol!
    
    func verifyPhoneAPI() {
        RequestManager.defaultManager.WebServiceApiRequest(service: Endpoints.verifyPhone(apiVersion: 1), parameters: nil) { (respone: VerifyPhoneResponse?, error: Bool) in
            if !error {
                if let res = respone, res.status == 200 {
                    self.verifyPhoneDelegate?.onSuccessVerifyPhone(response: res)
                } else {
                    self.verifyPhoneDelegate?.onFailuarVerifyPhone(errorMessage: "phone not verified from back end")
                }
            } else {
                if !ReachabilityChecker.shared.isConnectedToNetwork() {
                    self.verifyPhoneDelegate.onConnectionError()
                }
            }
        }
    }
    
    
}


protocol onVerifyPhoneModelToPresenterProtocol {
    func onSuccessVerifyPhone(response: VerifyPhoneResponse)
    func onFailuarVerifyPhone(errorMessage:String)
    func onConnectionError()
}
