//
//  PassengersOnMapViewController.swift
//  TerLive
//
//  Created by NewUser on 3/3/20.
//  Copyright © 2020 TerLive.com. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class PassengersOnMapViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var mapContainerView: UIView!
    @IBOutlet weak var currentLocationButton: UIButton!
    @IBOutlet weak var usersCollectionView: UICollectionView!
    
    var mapView: GMSMapView!
    var locationManager = CLLocationManager()
    var camera = GMSCameraPosition()
    var marker: GMSMarker!
    var navigateMarker: GMSMarker?
    var localPlaceMarker: GMSMarker?
    
    var localValue: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 29.973764, longitude: 30.914256)
    
    var passengersList = [CLLocation]()
    var passengersPoints: LocationsModel?
    var oldLocation: CLLocation!
    var newLocation: CLLocation!
    
    // variables for showing driver location to the parent
    var carMarker:GMSMarker!
    var oldDriverLocation: CLLocation!
    var newDriverLocation: CLLocation!
    
    
    //Mark:- location history from update location manger to draw acctual route
    var locationHistory = [CLLocation]() {
        didSet {
            
            if let lastIndex = locationHistory.last {
                let last = locationHistory.filter({$0 != lastIndex})
                if let lastOne = last.last {
                    self.oldLocation = lastIndex
                    self.newLocation = lastOne
                }
            }
            drawOnMapAndRotatCar(locationHistory)
        }
    }
    
    var driverLocationHistory = [CLLocation]() {
        didSet {
            
            if let lastIndex = driverLocationHistory.last {
                let last = driverLocationHistory.filter({$0 != lastIndex})
                if let lastOne = last.last {
                    self.oldDriverLocation = lastIndex
                    self.newDriverLocation = lastOne
                }
            }
            
            if let lastCoordinate = driverLocationHistory.last {
                self.rotatCarFront(lastCoordinate)
            }
            
        }
    }
    
    
//    var tripId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usersCollectionView.delegate = self
        usersCollectionView.dataSource = self
        setupMapView()
//        observeLiveLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        self.navigationController?.isNavigationBarHidden = true
        handleDrawLineOnMap()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if UserInfo.currentUser()?.type == "user" {
            self.navigationController?.isNavigationBarHidden = true
        }
        
    }
    
    @IBAction func resetCurrentLocationTapped(_ sender: UIButton) {
       resetCurrentLocation()
    }
    
    @IBAction func dismissTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func endTripTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func resetCurrentLocation(){
        //Mark:- animate and center camera between two positions
        guard let lastIndex = passengersList.last else { return }
        
        let bounds = GMSCoordinateBounds(coordinate: passengersList[0].coordinate, coordinate: lastIndex.coordinate)
        let cameraWithPadding: GMSCameraUpdate = GMSCameraUpdate.fit(bounds, withPadding: 50.0) //(will put inset the bounding box from the view's edge)
        
        self.mapView.animate(with: cameraWithPadding)
    }
    
    
    func handleDrawLineOnMap(){
        getDateFromXmlFile()
        drawOnMap(passengersList)
        
        // Static users Markers
        if UserInfo.currentUser()?.type == "avatar" {
            let userLocation = CLLocationCoordinate2D(latitude: 29.975550, longitude: 30.921424)
            addMarkerTo(location: userLocation, image: UIImage(named: "bluePointMarker")!)
            
        } else {
            addMarkerTo(location: passengersList[0].coordinate,image: UIImage(named: "currentLocation")!)
            addMarkerTo(location: CLLocationCoordinate2D(latitude: 29.982181, longitude: 30.945431), image: UIImage(named: "marker")!)
            addMarkerTo(location: CLLocationCoordinate2D(latitude: 29.992973, longitude: 30.935104), image: UIImage(named: "marker-1")!)
            addMarkerTo(location: CLLocationCoordinate2D(latitude: 29.982619, longitude: 30.919768), image: UIImage(named: "marker-2")!)
            addMarkerTo(location: passengersList.last!.coordinate,image: UIImage(named: "Pin")!)
        }
        
    }
    
    func setupMapView(){
        // Requet Location Access
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = 1
            locationManager.allowsBackgroundLocationUpdates = true
            locationManager.startUpdatingLocation()
        }
        
        mapView = GMSMapView.map(withFrame: self.view.bounds, camera: camera)
        mapView.delegate = self
        mapView.isMyLocationEnabled = false //blue user location
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = false
        mapView.isTrafficEnabled = false
        googleMapStyle()
        mapContainerView.addSubview(mapView)
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        if let userLocation = locations.last {
//
//
//            if UserInfo.currentUser()?.type == AppUserType.parent.rawValue {
//
//                //            if let userLocation = locations.last {
//                //                locationHistory.append(userLocation)
//                //            }
//
//
//
//            } else if UserInfo.currentUser()?.type == AppUserType.driver.rawValue {
//                if SocketIOManager.sharedInstance.checkSocketConnection() == .connected {
//                    if let id  = sharedTripId {
//                        self.sendLiveLocation(tripId: id, latitued: "\(userLocation.coordinate.latitude)", longitude: "\(userLocation.coordinate.longitude)")
//                    }
//                }
//            }
//
//
//            locationHistory.append(userLocation)
//            if let id = sharedTripId {
//                if SocketIOManager.sharedInstance.checkSocketConnection() == .connected {
//                    updateUserLocation(tripId: id, latitued: "\(userLocation.coordinate.latitude)", longitude: "\(userLocation.coordinate.longitude)")
//                }
//            }
//
//        }
        
        
        
    }
    
    func locationPermissionAgain(){
        // initialise a pop up for using later
        let alertController = UIAlertController(title: "Alert".localized, message: "Please go to Settings and turn on the permissions".localized, preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings".localized, style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in })
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .default, handler: nil)
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        
        // check the permission status
        switch(CLLocationManager.authorizationStatus()) {
        case .authorizedAlways, .authorizedWhenInUse:
            debugPrint("Authorize.")
        // get the user location
        case .notDetermined, .restricted, .denied:
            // redirect the users to settings
            self.present(alertController, animated: true, completion: nil)
        @unknown default:
            debugPrint("else")
        }
    }
    
    //Mark:- Add markers on map with user profile image
    func addMarkerTo(location: CLLocationCoordinate2D, image: UIImage = UIImage(named: "bluePointMarker")!) {
        
        let position = location
        let marker = GMSMarker(position: position)
        //marker.title = title
        marker.icon = image
        marker.tracksViewChanges = true
        marker.map = mapView
        
        localPlaceMarker = marker
    }
    
    //Mark:- Change Google Map Style with json file
    func googleMapStyle(){
        do {
            if let styleURL = Bundle.main.url(forResource: "googlemap_style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                debugPrint("Unable to find style.json")
            }
        } catch {
            debugPrint("The style definition could not be loaded: \(error)")
        }
    }
    
    
    //Mark:- Bearing car front
    func rotatCarFront(_ location: CLLocation){
        
        //add marker
        mapView.clear()
        carMarker = GMSMarker(position: location.coordinate)
        carMarker.icon = UIImage(named: "currentLocationBtn")
        carMarker.tracksViewChanges = true
        
        //get location angle to rotate car
        if let oldL = oldDriverLocation, let newL = newDriverLocation {
            let degrees = oldL.bearingDegreesTo(location: newL)
            carMarker.rotation = degrees
        }
        
        carMarker.map = mapView
        
        let camera = GMSCameraPosition.camera(withLatitude: driverLocationHistory.last!.coordinate.latitude, longitude: driverLocationHistory.last!.coordinate.longitude, zoom: 16)
        
        mapView.animate(to: camera)
        
    }
    
}

extension PassengersOnMapViewController {
    
    //Draw Line on map by local or acctual value from user update location delegate
    func drawOnMap(_ locations: [CLLocation]){
        let path = GMSMutablePath()
        for location in locations {
            path.addLatitude(location.coordinate.latitude, longitude: location.coordinate.longitude)
            
            //add marker
            mapView.clear()
            marker = GMSMarker(position: location.coordinate)
            marker.icon = UIImage(named: "bluePointMarker")
            marker.tracksViewChanges = true
            marker.map = mapView
        }
        
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 4.0
        polyline.strokeColor = UIColor.init(hexString: shadowForBlueButton, alpha: 1)
        polyline.map = mapView
    }
    
    //Draw Line on map by local or acctual value from user update location delegate
    func drawOnMapAndRotatCar(_ locations: [CLLocation]){
        for location in locations {
            //add marker
            navigateMarker?.map = nil
            navigateMarker = GMSMarker(position: location.coordinate)
            navigateMarker?.icon = UIImage(named: "currentLocationBtn")
            navigateMarker?.tracksViewChanges = true
            
            //get location angle to rotate car
            if let oldL = oldLocation, let newL = newLocation {
                let degrees = oldL.bearingDegreesTo(location: newL)
                navigateMarker?.rotation = degrees
            }
            
            navigateMarker?.map = mapView
            
        }
        // change camera position for new location
        let camera = GMSCameraPosition.camera(withLatitude: locationHistory.last!.coordinate.latitude, longitude: locationHistory.last!.coordinate.longitude, zoom: 16)
        mapView.animate(to: camera)
        
    }
}

extension PassengersOnMapViewController {
    func updateUserLocation(tripId: String, latitued: String, longitude: String){
//        SocketIOManager.sharedInstance.updateLocationScoket(tripId, latitued, longitude) { (res: ScocketResponse) in
//            DispatchQueue.main.async {
//                debugPrint(res.socketNumber)
//                
//            }
//        }
    }
    
    func sendLiveLocation(tripId: String, latitued: String, longitude: String){
        DispatchQueue.main.async {
//            SocketIOManager.sharedInstance.sendLocationScoket(latitued, longitude)
        }
    }
    
//    func observeLiveLocation(){
//        SocketIOManager.sharedInstance.observeLocationScoket() { (res: ScocketResponse) in
//            DispatchQueue.main.async {
//                debugPrint(res.socketNumber)
//
//                if let lat = res.latitude , let long = res.longitude {
//                    debugPrint("location from socket lat: \(lat) long \(long)")
//                    let x = CLLocation(latitude: Double(lat)!, longitude: Double(long)!)
//                    self.driverLocationHistory.append(x)
//                }
//
//            }
//        }
//    }
    
    
}

extension PassengersOnMapViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PassengersOnMapCell", for: indexPath) as! PassengersOnMapCell
        
        return cell
    }
    
    
}

extension PassengersOnMapViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let yourWidth = (usersCollectionView.frame.width) / 1.4
        let yourHeight = CGFloat(110)
        return CGSize(width: yourWidth, height: yourHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
}




//extension CLLocation {
//
//    func getRadiansFrom(degrees: Double ) -> Double {
//        return degrees * .pi / 180
//    }
//
//    func getDegreesFrom(radians: Double) -> Double {
//        return radians * 180 / .pi
//    }
//
//
//    func bearingRadianTo(location: CLLocation) -> Double {
//        
//        let lat1 = self.getRadiansFrom(degrees: self.coordinate.latitude)
//        let lon1 = self.getRadiansFrom(degrees: self.coordinate.longitude)
//
//        let lat2 = self.getRadiansFrom(degrees: location.coordinate.latitude)
//        let lon2 = self.getRadiansFrom(degrees: location.coordinate.longitude)
//
//        let dLon = lon2 - lon1
//
//        let y = sin(dLon) * cos(lat2)
//        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
//
//        var radiansBearing = atan2(y, x)
//
//        if radiansBearing < 0.0 {
//            radiansBearing += 2 * .pi
//        }
//
//        return radiansBearing
//    }
//
//    func bearingDegreesTo(location: CLLocation) -> Double {
//        return self.getDegreesFrom(radians: self.bearingRadianTo(location: location))
//    }
//
//
//}

extension PassengersOnMapViewController: XMLParserDelegate {
    //Private method
    func getDateFromXmlFile(){
        if let path = Bundle.main.path(forResource: "wayPointsJson", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                if let decoded = try? JSONDecoder().decode(LocationsModel.self, from: data) {
//                    debugPrint("decoded:", decoded)
                    self.passengersPoints = decoded
                    
                    for i in (passengersPoints?.points!)! {
                        if let lat = Double(i.lat!), let long = Double(i.long!) {
                            let location = CLLocation(latitude: lat,longitude: long)
                            self.passengersList.append(location)
                            
                        }
                    }
                }
            } catch {
//                debugPrint("error to decode json")
            }
        }
    }
    
}

struct LocationsModel : Codable {
    
    let points : [LocationsPoint]?
    
    enum CodingKeys: String, CodingKey {
        case points = "points"
    }
    
}

struct LocationsPoint : Codable {
    
    let lat : String?
    let long : String?
    
    enum CodingKeys: String, CodingKey {
        case lat = "lat"
        case long = "long"
    }
}



