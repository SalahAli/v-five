//
//  MapPickupLocationViewController.swift
//  TerLive
//
//  Created by Ahmed on 3/4/20.
//  Copyright © 2020 TerLive.com. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class MapPickupLocationViewController: UIViewController {
    
    @IBOutlet weak var mapContainerView: UIView!
    @IBOutlet var pinImage: UIImageView!
    @IBOutlet weak var confirmLocationButton: UIButton!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    
    var geoCoder = CLGeocoder()
    var mapView: GMSMapView!
    var locationManager = CLLocationManager()
    var camera = GMSCameraPosition()
    var marker: GMSMarker!
    
    
    var localValue: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        PickupLocationPresenter.shared.PickupLocationDelegate = self
//        PickupLocationPresenter.shared.viewDidLoadIsFinished()
        addGraidentColorTo(button: confirmLocationButton)
        setupMapView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let backImage = UIImage(named: "backArrow")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationController?.isNavigationBarHidden = false
    }

  
    func setupMapView(){
        // Requet Location Access
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = 1000
            locationManager.allowsBackgroundLocationUpdates = true
            locationManager.startUpdatingLocation()
            //            locationManager.startUpdatingHeading()
        }
        
        mapView = GMSMapView.map(withFrame: self.view.bounds, camera: camera)
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.isTrafficEnabled = false
        googleMapStyle()
        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 80, right: 10)
        
        mapContainerView.addSubview(mapView)
        mapContainerView.bringSubviewToFront(pinImage)
        mapContainerView.bringSubviewToFront(confirmLocationButton)
        mapContainerView.bringSubviewToFront(searchView)
        bottomView()
        
    }
    
    func bottomView(){
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 150))
        view.backgroundColor = .white
        view.layer.cornerRadius = 30.0
//        self.view.addSubview(view)
        mapContainerView.bringSubviewToFront(view)
        confirmLocationButton.clipsToBounds = false
        confirmLocationButton.layer.shadowOpacity = 0.5
        confirmLocationButton.layer.cornerRadius = 10.0
        confirmLocationButton.layer.shadowRadius = 1.0
        confirmLocationButton.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        confirmLocationButton.layer.shadowColor = UIColor.orange.cgColor
        
        self.maskLayerWith(inputView: view)
    }
    
    func maskLayerWith(inputView:UIView) {
        let finalPath = UIBezierPath(rect: inputView.bounds)
        let maskLayer = CAShapeLayer()
        maskLayer.frame = inputView.bounds
        finalPath.append(self.rectanglePath())
        maskLayer.fillRule = CAShapeLayerFillRule.evenOdd
        maskLayer.path = finalPath.cgPath
        inputView.layer.mask = maskLayer
    }
    
    func rectanglePath() -> UIBezierPath {
        let rect = CGRect(x: 50, y: 100, width: 280, height: 100)
        return UIBezierPath(rect: rect)
    }
    
    // Handle fixed pin on map
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("map view draggrd")
        
        let lat = position.target.latitude
        let lng = position.target.longitude
        
        // Create pickup Location
        
            let location = CLLocation(latitude: lat, longitude: lng)
            localValue = location.coordinate
            getAddressTextby(location)
        
       
        
        
    }
    
    func getAddressTextby(_ location: CLLocation){
        // Geocode Location
        geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
            if let placemarks = placemarks {
                if let location = placemarks.first?.location {
                    if let addressDict = (placemarks.first?.addressDictionary as? NSDictionary){
                        if let city = addressDict["City"] as? String, let addressName = addressDict["Name"] as? String {
                            print(addressName)
                           
                            self.searchTextField.text = "\(addressName) \(city)"
                            sharedAddress = "\(addressName) \(city)"
                        }
                    }
                }
            }
            
        }
    }
    
    //Mark:- Change Google Map Style with json file
    func googleMapStyle(){
        do {
            if let styleURL = Bundle.main.url(forResource: "googlemap_style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            }
        } catch {
            debugPrint("The style definition could not be loaded: \(error)")
        }
    }
    
    @IBAction func pickupLocationTapped(_ sender: UIButton) {
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                debugPrint("No access")
                locationPermissionAgain()
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                // Update location from BackEnd After active location Authorization
                DispatchQueue.main.async {
                    if let portalId = UserInfo.currentUser()?.associationId {
//                        PickupLocationPresenter.shared.pickupLocationButtonPressed(associationId: portalId, latitude: "\(self.localValue.latitude)", longitude: "\(self.localValue.longitude)", address: self.searchTextField.text!)
                    }
                    sharedLat = "\(self.localValue.latitude)"
                    sharedLong = "\(self.localValue.longitude)"
                    self.navigationController?.popViewController(animated: true)
                }
            }
        } else {
            debugPrint("Location services are not enabled")
            locationPermissionAgain()
        }
    }
    
    func addGraidentColorTo(button: UIButton) {
        let gradient: CAGradientLayer = CAGradientLayer()
        //            gradient.startPoint = CGPoint(x: 1.0, y: 1.0)
        //            gradient.endPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.startPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.colors = [UIColor.init(hexString: darkBlue, alpha: 0.8).cgColor,
                           UIColor.init(hexString: darkGreen, alpha: 0.8).cgColor,
                           UIColor.init(hexString: green, alpha: 1.0).cgColor,
                           UIColor.init(hexString: green, alpha: 1.0).cgColor,
                           UIColor.init(hexString: yellow, alpha: 0.9 ).cgColor]
        gradient.locations = [0.0 ,0.25, 0.4, 0.6, 1.0]
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: confirmLocationButton.frame.size.width, height: confirmLocationButton.frame.size.height)
        gradient.cornerRadius = 4.5
        button.layer.insertSublayer(gradient, at: 0)
        button.setTitleColor(UIColor.white, for: .normal)
    }
    
    
    
}

extension MapPickupLocationViewController: GMSMapViewDelegate, CLLocationManagerDelegate {
    
    func locationPermissionAgain(){
        // initialise a pop up for using later
        let alertController = UIAlertController(title: "Alert".localized, message: "Please go to Settings and turn on the permissions".localized, preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings".localized, style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in })
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .default, handler: nil)
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        
        // check the permission status
        switch(CLLocationManager.authorizationStatus()) {
        case .authorizedAlways, .authorizedWhenInUse:
            print("Authorize.")
        // get the user location
        case .notDetermined, .restricted, .denied:
            // redirect the users to settings
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // Current Location
        localValue = manager.location!.coordinate
        print("User Update Location at Lat\(localValue.latitude): Long\(localValue.longitude)")
        self.mapView.clear()
        
        // Add Marker
        let marker = GMSMarker()
        marker.map = self.mapView
        let camera = GMSCameraPosition.camera(withLatitude: localValue.latitude, longitude: localValue.longitude, zoom: 16)
        self.mapView.camera = camera
        
    }
    
    //Mark:- Calc destance between source and destination
    func calcDistance(_ origin: CLLocation, _ destination: CLLocation)-> Double {
        let coordinate0 = origin
        let coordinate1 = destination
        let distanceInMeters = coordinate0.distance(from: coordinate1)
        let disKM = distanceInMeters / 1000
        print("distanceInMeters is: \(disKM) km")
        return disKM
    }
    
    //To DO: I will use it when user change autorization when he open the app
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
}


extension CLLocation {
    
    func getRadiansFrom(degrees: Double ) -> Double {
        return degrees * .pi / 180
    }
    
    func getDegreesFrom(radians: Double) -> Double {
        return radians * 180 / .pi
    }
    
    
    func bearingRadianTo(location: CLLocation) -> Double {
        
        let lat1 = self.getRadiansFrom(degrees: self.coordinate.latitude)
        let lon1 = self.getRadiansFrom(degrees: self.coordinate.longitude)
        
        let lat2 = self.getRadiansFrom(degrees: location.coordinate.latitude)
        let lon2 = self.getRadiansFrom(degrees: location.coordinate.longitude)
        
        let dLon = lon2 - lon1
        
        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        
        var radiansBearing = atan2(y, x)
        
        if radiansBearing < 0.0 {
            radiansBearing += 2 * .pi
        }
        
        return radiansBearing
    }
    
    func bearingDegreesTo(location: CLLocation) -> Double {
        return self.getDegreesFrom(radians: self.bearingRadianTo(location: location))
    }
    
    
}

extension MapPickupLocationViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == searchTextField {
            view.endEditing(true)
            
            return false
        }
        return true
    }
}
