//
//  HomeModel.swift
//  V-Five
//
//  Created by Ahmed on 6/23/20.
//  Copyright © 2020 V-Five. All rights reserved.
//

import Foundation

class HomeModel {
    
    static let shared = HomeModel()
    var phoneIsExistDelegate: onPhoneIsExistModelToPresenterProtocol!
    
    func login(phoneNumber: String) {
        let param: [String:Any] = ["phone": phoneNumber]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: param, options: [])
        guard let jsonString = String(data: jsonData!, encoding: .utf8) else { return }
        
        RequestManager.defaultManager.WebServiceApiRequest(service: Endpoints.checkIsPhoneExist(apiVersion: 1), parameters: jsonString) { (respone: PhoneExistResponse?, error: Bool) in
            if !error {
                if respone?.status == 200 {
                    if let res = respone {
                        self.phoneIsExistDelegate.onSuccessPhoneIsExist(response: res)
                    }
                } else {
                    self.phoneIsExistDelegate?.onFailuarLogin(errorMessage: "User doesn't exist")
                }
                
            } else {
                if !ReachabilityChecker.shared.isConnectedToNetwork() {
                    self.phoneIsExistDelegate.onConnectionError()
                }
            }
        }
    }
}

protocol onPhoneIsExistModelToPresenterProtocol{
    func onSuccessPhoneIsExist(response: PhoneExistResponse)
    func onFailuarLogin(errorMessage: String)
    func onConnectionError()
}

