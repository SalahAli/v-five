//
//  CreateOutingModel.swift
//  V-Five
//
//  Created by Ahmed on 6/27/20.
//  Copyright © 2020 V-Five. All rights reserved.
//

import Foundation

class CreateOutingModel {
    
    static let shared = CreateOutingModel()
    var createOutingDelegate: onCreateOutingModelToPresenterProtocol!
    var getTagsDelegate: onGetTagsModelToPresenterProtocol!
    
    func isPhoneExist(phoneNumber: String) {
        let param: [String:Any] = ["phone": phoneNumber]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: param, options: [])
        guard let jsonString = String(data: jsonData!, encoding: .utf8) else { return }
        
        RequestManager.defaultManager.WebServiceApiRequest(service: Endpoints.checkIsPhoneExist(apiVersion: 1), parameters: jsonString) { (respone: PhoneExistResponse?, error: Bool) in
            if !error {
                if respone?.status == 200 {
                    if let res = respone {
                        self.createOutingDelegate?.onSuccessPhoneIsExist(response: res)
                    }
                } else {
                    self.createOutingDelegate?.onFailuarLogin(errorMessage: "User doesn't exist")
                }
                
            } else {
                if !ReachabilityChecker.shared.isConnectedToNetwork() {
                    self.createOutingDelegate.onConnectionError()
                }
            }
        }
    }
    
    func getTages() {
        
        RequestManager.defaultManager.WebServiceApiRequest(service: Endpoints.getAllTags(apiVersion: 1), parameters: nil) { (respone: TagsResponse?, error: Bool) in
            if !error {
                if respone?.status == 200 {
                    if let res = respone {
                        self.getTagsDelegate?.onSuccessGetTags(response: res)
                    }
                } else {
                    self.getTagsDelegate?.onFailuarGetTags(errorMessage: "error get tags")
                }
                
            } else {
                if !ReachabilityChecker.shared.isConnectedToNetwork() {
                    self.getTagsDelegate?.onConnectionErrorGetTags()
                }
            }
        }
    }
    
    func createNewOuting(outingData: CreateNewOutingRequest, placeName: String, description: String, latitude: String, longitude: String,address: String, friends: [String],tagId: String){
        
//        let param: [String:Any] = ["placeName" : placeName,
//                                   "description" : description,
//                                   "latitude" : latitude,
//                                   "longitude" : longitude,
//                                   "address" : address,
//                                   "friends" : friends,
//                                   "tagId" : tagId]
        

        
        guard let jsonData = try? JSONEncoder().encode(outingData) else { return }
//        let jsonData = try? JSONSerialization.data(withJSONObject: param, options: [])
        guard let jsonString = String(data: jsonData, encoding: .utf8) else { return }
        debugPrint(jsonString)

//        "{\"address\":\"12556 6th Of October\",\"longitude\":\"30.913839563727375\",\"friends\":[{\"id\":\"5ecdfb9b39af8049919d502d\",\"phone\":\"01142849437\"}],\"latitude\":\"29.97525685379478\",\"placeName\":\"12556 6th Of October\",\"tagId\":\"5ed680c82fb7872245761eaf\",\"description\":\"Yyyyyyy\"}"
        
        
//        "{\"address\":\"12556 6th Of October\",\"longitude\":\"30.91974712908268\",\"friends\":[{\"id\":\"5f064799299a42bb0779cdd6\",\"phone\":\"01142849437\"}],\"latitude\":\"29.988811168745777\",\"placeName\":\"12556 6th Of October\",\"tagId\":\"5ed680c82fb7872245761eaf\",\"description\":\"Rrrrrr\"}"
        
        
//        {"address":"12556 6th Of October","longitude":"30.91974712908268","friends":[{"id":"5f064799299a42bb0779cdd6","phone":"01142849437"}],"latitude":"29.988811168745777","placeName":"12556 6th Of October","tagId":"5ed680c82fb7872245761eaf","description":"Rrrrrr"}
        
        RequestManager.defaultManager.WebServiceApiRequest(service: Endpoints.createNewTrip(apiVersion: 1), parameters: jsonString) { (respone: MainResponse?, error: Bool) in
            if !error {
                if respone?.status == 200 {
                    if let res = respone {
                        self.createOutingDelegate?.onSuccessCreateOuting(response: res)
                    }
                } else {
                    self.createOutingDelegate?.onFailuarCreateOuting(errorMessage: respone?.message ?? "")
                }
                
            } else {
                if !ReachabilityChecker.shared.isConnectedToNetwork() {
                    self.createOutingDelegate?.onConnectionErrorCreateOuting()
                }
            }
        }
    }
    
    
    
    
    
}

protocol onCreateOutingModelToPresenterProtocol{
    func onSuccessPhoneIsExist(response: PhoneExistResponse)
    func onFailuarLogin(errorMessage: String)
    func onConnectionError()
    
    func onSuccessCreateOuting(response: MainResponse)
    func onFailuarCreateOuting(errorMessage: String)
    func onConnectionErrorCreateOuting()
}

protocol onGetTagsModelToPresenterProtocol {
    func onSuccessGetTags(response: TagsResponse)
    func onFailuarGetTags(errorMessage: String)
    func onConnectionErrorGetTags()
}



