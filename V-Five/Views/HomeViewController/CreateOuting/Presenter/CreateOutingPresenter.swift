//
//  CreateOutingPresenter.swift
//  V-Five
//
//  Created by Ahmed on 6/27/20.
//  Copyright © 2020 V-Five. All rights reserved.
//

import Foundation

class CreateOutingPresenter: onCreateOutingModelToPresenterProtocol, onGetTagsModelToPresenterProtocol {
    
    static let shared = CreateOutingPresenter()
    var createOutingDelegate: onCreateOutingPresenterToViewProtocol!
    var getTagsDelegate: onGetTagsPresenterToViewProtocol!
    
    func onConnectionError() {
        createOutingDelegate.showConnectionErro()
    }
    
    func viewDidLoadIsFinished() {
        CreateOutingModel.shared.createOutingDelegate = self
        CreateOutingModel.shared.getTagsDelegate = self
    }
    
    func selectPhonePressed(phoneNumber: String) {
        CreateOutingModel.shared.isPhoneExist(phoneNumber: phoneNumber)
    }
    
    func onSuccessPhoneIsExist(response: PhoneExistResponse) {
        createOutingDelegate.onSuccessPhoneExist(response: response)
    }
    
    func onFailuarLogin(errorMessage: String) {
        createOutingDelegate.onFailuarPhoneExist(errorMessage: errorMessage)
    }
    
    // Get tags
    func getTages() {
        CreateOutingModel.shared.getTages()
    }
    
    func onSuccessGetTags(response: TagsResponse) {
        self.getTagsDelegate?.onSuccessGetTags(response: response)
    }
    
    func onFailuarGetTags(errorMessage: String) {
        self.getTagsDelegate?.onFailuarGetTags(errorMessage: errorMessage)
    }
    
    func onConnectionErrorGetTags() {
        self.getTagsDelegate?.onConnectionErrorGetTags()
    }
    
    //create outing
    func createNewOuting(outingData: CreateNewOutingRequest, placeName: String, description: String, latitude: String, longitude: String,address: String, friends: [String],tagId: String){
        CreateOutingModel.shared.createNewOuting(outingData: outingData, placeName: placeName, description: description, latitude: latitude, longitude: longitude, address: address, friends: friends, tagId: tagId)
    }
    
    func onSuccessCreateOuting(response: MainResponse) {
        self.createOutingDelegate?.onSuccessCreateOuting(response: response)
    }
    
    func onFailuarCreateOuting(errorMessage: String) {
        self.createOutingDelegate?.onFailuarCreateOuting(errorMessage: errorMessage)
    }
    
    func onConnectionErrorCreateOuting() {
        self.createOutingDelegate?.onConnectionErrorCreateOuting()
    }
    
}

protocol onCreateOutingPresenterToViewProtocol{
    func onSuccessPhoneExist(response: PhoneExistResponse)
    func onFailuarPhoneExist(errorMessage: String)
    func showConnectionErro()
    
    func onSuccessCreateOuting(response: MainResponse)
    func onFailuarCreateOuting(errorMessage: String)
    func onConnectionErrorCreateOuting()
}

protocol onGetTagsPresenterToViewProtocol{
    func onSuccessGetTags(response: TagsResponse)
    func onFailuarGetTags(errorMessage: String)
    func onConnectionErrorGetTags()
}



