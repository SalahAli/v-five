//
//  CreateOutingResponse.swift
//  V-Five
//
//  Created by Ahmed on 6/27/20.
//  Copyright © 2020 V-Five. All rights reserved.
//

import Foundation

struct PhoneExistResponse : Codable {
    
    let data : PhoneExistData?
    let message : String?
    let status : Int?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case message = "message"
        case status = "status"
    }
}

struct PhoneExistData : Codable {
    let id: String?
    let email : String?
    let fcmToken : String?
    let phoneValue : String?
    let phoneVerified : Bool?
    let profileImage : String?
    let username : String?
    
    enum CodingKeys: String, CodingKey {
        case id = "userId"
        case email = "email"
        case fcmToken = "fcmToken"
        case phoneValue = "phoneValue"
        case phoneVerified = "phoneVerified"
        case profileImage = "profileImage"
        case username = "username"
    }
    
}

//Tages Response

struct TagsResponse : Codable {
    
    let data : [TagsData]?
    let message : String?
    let status : Int?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case message = "message"
        case status = "status"
    }
}

struct TagsData : Codable {
    
    let id : String?
    let name : String?
    let color: String = AppYellow
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name = "name"
        case color = "color"
    }
    
}

struct CreateNewOutingRequest: Codable {
    
    var placeName = ""
    var placeDescription = ""
    var latitude = ""
    var longitude = ""
    var address = ""
    var friends = [FriendData]()
    var tagId = ""
    
    enum CodingKeys:String,CodingKey {
        
        case placeName = "placeName"
        case placeDescription = "description"
        case latitude = "latitude"
        case longitude = "longitude"
        case  address = "address"
        case  friends = "friends"
        case  tagId = "tagId"
    }
    
}

struct FriendData: Codable {
    
    var friendId = ""
    var phoneNumber = ""
    
    enum CodingKeys:String,CodingKey {
        case friendId = "id"
        case phoneNumber = "phone"
    }

    
}
