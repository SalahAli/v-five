//
//  ContactTableViewCell.swift
//  V-Five
//
//  Created by test on 15/05/2020.
//  Copyright © 2020 V-Five. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var phoneNumbersTableView: UITableView!
    @IBOutlet weak var phoneNumbersTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var contactName: UILabel!
    @IBOutlet weak var lettersLabel: UILabel!
    
    var phoneNumbers: [String] = []
    var selectedPhones: [String] = []
    var userData: PhoneExistData?
//    var contactsDelegate: SelectContactsDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        phoneNumbersTableView.dataSource = self
        phoneNumbersTableView.delegate = self
        CreateOutingPresenter.shared.createOutingDelegate = self
        CreateOutingPresenter.shared.viewDidLoadIsFinished()
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    func configureNumberCell(phones: [String]) {
        phoneNumbers = phones
        phoneNumbersTableView.reloadData()
    }
}


extension ContactTableViewCell: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return phoneNumbers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PhoneNumberCell", for: indexPath) as? PhoneNumberTableViewCell {
            cell.selectionStyle = .none
            cell.phoneNumber.text = phoneNumbers[indexPath.row]
            cell.checkBoxImage.image = UIImage(named: "blank_checkBox")
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 25
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("\(indexPath.row)")
        
        if let cell = phoneNumbersTableView.cellForRow(at: IndexPath(row: indexPath.row, section: 0)) as? PhoneNumberTableViewCell {
            if selectedPhones.contains(phoneNumbers[indexPath.row]) {
                cell.checkBoxImage.image = UIImage(named: "blank_checkBox")
                if let index = selectedPhones.firstIndex(of: phoneNumbers[indexPath.row]) {
                    selectedPhones.remove(at: index)
                }
            } else {
                cell.checkBoxImage.image = UIImage(named: "selectedCheckBox")
                let phone = phoneNumbers[indexPath.row].replacingOccurrences(of: " ", with: "")
                selectedPhones.append(phone)
                debugPrint(phone)
                CreateOutingPresenter.shared.selectPhonePressed(phoneNumber: phone)
            }
        }
        
    }
}

extension ContactTableViewCell: onCreateOutingPresenterToViewProtocol {
    func onSuccessCreateOuting(response: MainResponse) {
        
    }
    
    func onFailuarCreateOuting(errorMessage: String) {
        
    }
    
    func onConnectionErrorCreateOuting() {
        
    }
    
    func onSuccessPhoneExist(response: PhoneExistResponse) {
        DispatchQueue.main.async {
            if let res = response.data {
                self.userData = res
                debugPrint(sharedContactsList.count)
                sharedContactsList.append(res)
                debugPrint(sharedContactsList.count)
            }
        }
    }
    
    func onFailuarPhoneExist(errorMessage: String) {
        
        debugPrint(errorMessage)
    }
    
    func showConnectionErro() {
        
    }
    
    
}

//protocol SelectContactsDelegate: class {
//    func getContactsList(_ contacts: [String])
//}
