//
//  TagTableViewCell.swift
//  V-Five
//
//  Created by test on 10/05/2020.
//  Copyright © 2020 V-Five. All rights reserved.
//

import UIKit

class TagTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tagView: UIView!
    @IBOutlet weak var tagName: UILabel!
    @IBOutlet weak var radioButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func configure(index: TagsData) {
        selectionStyle = .none
        tagView.backgroundColor = UIColor.init(hexString: index.color, alpha: 0.1)
        tagName.textColor =  UIColor.init(hexString: index.color, alpha: 1.0)
        radioButton.setImage(UIImage(named: "radio_NotActive"), for: .normal)
        tagName.text = index.name ?? ""
        
    }
    
}
