//
//  CreateOutingDataSource.swift
//  V-Five
//
//  Created by Ahmed on 6/27/20.
//  Copyright © 2020 V-Five. All rights reserved.
//

import UIKit

extension CreateOutingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tagsTableView {
            return tagsDataSource.count
        }
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tagsTableView {
            if tagsDataSource.count > 0 {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "TagCell", for: indexPath) as? TagTableViewCell {
                    cell.configure(index: tagsDataSource[indexPath.row])
                    return cell
                }
            }
         
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as? ContactTableViewCell {
                cell.selectionStyle = .none
                cell.phoneNumbersTableViewHeight.constant = CGFloat(contacts[indexPath.row].telephone.count * 25)
                cell.contactName.text = contacts[indexPath.row].firstName + " " + contacts[indexPath.row].lastName
//                cell.contactImage.image = UIImage(data: contacts[indexPath.row].image)
                cell.lettersLabel.text = String(contacts[indexPath.row].firstName.prefix(1) + contacts[indexPath.row].lastName.prefix(1))
                cell.configureNumberCell(phones: contacts[indexPath.row].telephone)
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tagsTableView {
            return 40
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tagsTableView {
            print("\(indexPath.row)")
            tagsTableView.reloadData()
            if let cell = tagsTableView.cellForRow(at: IndexPath(row: indexPath.row, section: 0)) as? TagTableViewCell{
                cell.radioButton.setImage(UIImage(named: "radio_active"), for: .normal)
                selectedTagColor = cell.tagView.backgroundColor
                selectedTagName = cell.tagName.text
                selectedTagNameColor = cell.tagName.textColor
                if let id = tagsDataSource[indexPath.row].id {
                    sharedTagId = id
                }
                
            }
        }
    }
}
