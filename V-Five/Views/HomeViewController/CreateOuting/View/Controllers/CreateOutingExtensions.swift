//
//  CreateOutingExtensions.swift
//  V-Five
//
//  Created by Ahmed on 7/6/20.
//  Copyright © 2020 V-Five. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

extension CreateOutingViewController {
    
    func setupViewController(){
           CreateOutingPresenter.shared.getTagsDelegate = self
           CreateOutingPresenter.shared.viewDidLoadIsFinished()
           registerKeyboardNotifications()
           descriptionTextView.smartInsertDeleteType = UITextSmartInsertDeleteType.no
           descriptionTextView.delegate = self
           [blackView,descriptionView,selectedTagView,tagsView,contactsView].forEach{ $0?.isHidden = true }
           [imag1,img2,img3].forEach{ $0?.isHidden = true }
           remainingNumber.isHidden = true
           hideBlackView()
           fetchedContacts()
           CreateOutingPresenter.shared.getTages()
           addGestureRecognizer(mapUIImageView)
       }
    
    func fetchedContacts(){
         // 1.
         let store = CNContactStore()
         store.requestAccess(for: .contacts) { (granted, error) in
             if let error = error {
                 debugPrint("failed to request access", error)
                 return
             }
             if granted {
                 // 2.
                 let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
                 let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                 do {
                     // 3.
                     try store.enumerateContacts(with: request, usingBlock: { (contact, stopPointer) in
                         var phones: [String] = []
                         for phone in contact.phoneNumbers {
                             phones.append(phone.value.stringValue)
                         }
                         let image = Data()
                         self.contacts.append(FetchedContact(firstName: contact.givenName, lastName: contact.familyName, telephone: phones, image: image))
                     })
                 } catch let error {
                     debugPrint("Failed to enumerate contact", error)
                 }
             } else {
                 debugPrint("access denied")
             }
         }
     }
}
