//
//  FetchedContact.swift
//  V-Five
//
//  Created by test on 16/05/2020.
//  Copyright © 2020 V-Five. All rights reserved.
//

import Foundation
import Contacts

struct FetchedContact {
    var firstName: String
    var lastName: String
    var telephone: [String]
    var image: Data
}
