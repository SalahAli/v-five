//
//  CreateOutingViewController.swift
//  V-Five
//
//  Created by test on 09/05/2020.
//  Copyright © 2020 V-Five. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

class CreateOutingViewController: UIViewController {
    
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var blurMapView: UIVisualEffectView!
    @IBOutlet weak var descriptionBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var descriptionBackGroundView: UIView!
    @IBOutlet weak var blackView: UIView!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionDetails: UILabel!
    @IBOutlet weak var tagsTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tagsView: UIView!
    @IBOutlet weak var tagsTableView: UITableView!
    @IBOutlet weak var tagsViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var selectedTagView: UIView!
    @IBOutlet weak var selectedTagLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var contactsView: UIView!
    @IBOutlet weak var contactTableView: UITableView!
    @IBOutlet weak var contactViewBottomConstraint: NSLayoutConstraint!
//    @IBOutlet weak var contactsTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var addToUpComingOutings: UIButton!
    @IBOutlet weak var addNumberView: UIView!
    
    @IBOutlet weak var contactsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var imag1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var remainingNumber: UILabel!
    @IBOutlet weak var mapUIImageView: UIImageView!
    @IBOutlet weak var contactsViewBottomConstraint: NSLayoutConstraint!
    
    var tagSelected: Bool = false
    var descriptionIsWritten: Bool = false
    var friendsSelected: Bool = false
    
    @IBOutlet weak var tagsBackgroundView: UIView!
    var descriptionViewIsActive: Bool = false
    var tagsViewIsActive: Bool = false
    var contactsViewIsActive: Bool = false
    var selectedTagColor: UIColor!
    var selectedTagName: String!
    var selectedTagNameColor: UIColor!
    
    //Injections
    var contacts: [FetchedContact] = []
    var tagsDataSource = [TagsData]()
    var friendsArray = [FriendData]()
    var outingObject = CreateNewOutingRequest()
    var friendObject = FriendData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewController()
        //        contactsViewController.contactsDelegate = self
        contactsViewHeight.constant = UIScreen.main.bounds.height - 100
        viewsSetupCorner()
    }
    
    func viewsSetupCorner() {
        contactsView.roundCorners(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner],
                                radius: 20.0,
                                borderWidth: 0,
                                borderColor: UIColor.clear,
                                shadowColor: UIColor.clear,
                                shadowOffsetWidth: 0,
                                shadowOffsetHeight: 0,
                                shadowOpicty: 0,
                                shadowRadius: 0)
        tagsBackgroundView.roundCorners(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner],
                                  radius: 20.0,
                                  borderWidth: 0,
                                  borderColor: UIColor.clear,
                                  shadowColor: UIColor.clear,
                                  shadowOffsetWidth: 0,
                                  shadowOffsetHeight: 0,
                                  shadowOpicty: 0,
                                  shadowRadius: 0)
        descriptionView.roundCorners(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner],
                                  radius: 20.0,
                                  borderWidth: 0,
                                  borderColor: UIColor.clear,
                                  shadowColor: UIColor.clear,
                                  shadowOffsetWidth: 0,
                                  shadowOffsetHeight: 0,
                                  shadowOpicty: 0,
                                  shadowRadius: 0)
        addNumberView.roundCorners(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner],
                                   radius: 20.0,
                                   borderWidth: 0.4,
                                   borderColor: UIColor.init(hexString: "viewBorderColor").withAlphaComponent(0.3),
                                   shadowColor: UIColor.clear,
                                   shadowOffsetWidth: 0,
                                   shadowOffsetHeight: 0,
                                   shadowOpicty: 0,
                                   shadowRadius: 0)
        
    }
    
    @IBAction func backButton(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    override func viewWillLayoutSubviews() {
        setupBlurView()
        addGradientLayer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
        self.view.layoutIfNeeded()
        tagsTableViewHeight.constant = tagsTableView.contentSize.height + 15
    }
    
    func setupDescriptionView() {
        self.view.clipsToBounds = false
        descriptionBackGroundView.layer.cornerRadius = 15
        descriptionBackGroundView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        descriptionBackGroundView.layer.masksToBounds = true
    }
    
    func setupBlurView() {
        self.view.clipsToBounds = false
        blurMapView.layer.cornerRadius = 15
        blurMapView.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMaxYCorner]
        blurMapView.layer.masksToBounds = true
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        descriptionDetails.text = descriptionTextView.text
        hideDescriptionView()
        descriptionViewIsActive = false
        if descriptionDetails.text != "" { descriptionIsWritten = true }
        checkTripButtons()
    }
    
    func hideBlackView() {
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(hideDescriptionView))
        blackView.addGestureRecognizer(tapGesture)
    }
    
    @IBAction func addTagButtonPressed(_ sender: Any) {
        hideDescriptionView()
        tagsViewIsActive = false
        selectedTagView.isHidden = false
        selectedTagView.backgroundColor = selectedTagColor
        selectedTagLabel.text = selectedTagName
        selectedTagLabel.textColor = selectedTagNameColor
        tagSelected = true
        checkTripButtons()
    }
//    this is parameters:{"address":"El Mostakbal -El Bashayer Road 6th Of October","longitude":"30.926844589412212","friends":[{"id":"5ecdfb9b39af8049919d502d","phone":"01142849437"}],"latitude":"29.978864800290765","placeName":"El Mostakbal -El Bashayer Road 6th Of October","tagId":"5ed680c82fb7872245761eaf","description":"Goooo now"}
    
   // this is parameters:{"address":"El Tahrir Road 6th Of October","longitude":"30.909306295216087","friends":[{"id":"5f064799299a42bb0779cdd6","phone":"01142849437"}],"latitude":"29.954508981763425","placeName":"El Tahrir Road 6th Of October","tagId":"5ed680c82fb7872245761eaf","description":"Test app"}
    
    
    
    // Mark:- Create New Outing
    @IBAction func addToUpComingTapped(_ sender: UIButton) {
        
        guard let lat = sharedLat, let long = sharedLong, let address = sharedAddress, let tagId =  sharedTagId else { return }
        
//        for i in sharedContactsList {
//            if let id = i.id, let phone = i.phoneValue {
//                friendObject.friendId = id
//                friendObject.phoneNumber = phone
//                friendsArray.append(friendObject)
//            }
//        }
        
        friendObject.friendId = "5f064799299a42bb0779cdd6"
        friendObject.phoneNumber = "01142849437"
        
        outingObject.friends.append(friendObject)
        outingObject.placeName = address
        outingObject.placeDescription = descriptionTextView.text!
        outingObject.latitude = lat
        outingObject.longitude = long
        outingObject.address = address
        outingObject.tagId = tagId
        
        debugPrint(outingObject)
        
        CreateOutingPresenter.shared.createNewOuting(outingData: outingObject, placeName: address, description: descriptionTextView.text!, latitude: lat, longitude: long, address: address, friends: ["ii"], tagId: tagId)
    }
    
    @objc func hideDescriptionView() {
        UIView.animate(withDuration: 0.2, animations: {
            self.blackView.isHidden = true
            self.descriptionViewIsActive = false
            self.tagsViewIsActive = false
            self.descriptionView.isHidden = true
            self.tagsView.isHidden = true
            self.contactsViewIsActive = false
            self.contactsView.isHidden = true
            self.view.endEditing(true)
            self.view.layoutIfNeeded()
        })
    }
    
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    // MARK: - Keyboard scrolling handling
    @objc func keyboardWillShow(notification: Notification) {
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if self.descriptionViewIsActive == true {
                    UIView.animate(withDuration: 0.2, animations: {
                        self.descriptionBottomConstraint.constant = keyboardHeight
                        self.view.layoutIfNeeded()
                    })
                }
            }
            if tagsViewIsActive == true {
                UIView.animate(withDuration: 0.3, animations: {
                    self.tagsViewBottomConstraint.constant = keyboardHeight
                    if keyboardHeight + self.tagsTableView.contentSize.height + 90 > UIScreen.main.bounds.height {
                        self.tagsTableViewHeight.constant = UIScreen.main.bounds.height - keyboardHeight - 120
                    } else {
                        self.tagsTableViewHeight.constant = self.tagsTableView.contentSize.height
                    }
                    self.view.layoutIfNeeded()
                })
            }
            if contactsViewIsActive == true {
                UIView.animate(withDuration: 0.3, animations: {
                    self.contactsViewHeight.constant = UIScreen.main.bounds.height - (keyboardHeight + 80)
                    self.contactsViewBottomConstraint.constant = keyboardHeight
                    self.view.layoutIfNeeded()
                })
            }
        }
        
    }
    
    
    @objc func keyboardWillBeHidden(notification: Notification) {
        self.contactsViewHeight.constant = UIScreen.main.bounds.height - 100
        self.contactsViewBottomConstraint.constant = 0
    }
    
    func addGradientLayer() {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.colors = [UIColor.init(hexString: darkGreen, alpha: 0.3).cgColor,
                           UIColor.init(hexString: green, alpha: 1.0).cgColor,
                           UIColor.init(hexString: green, alpha: 1.0).cgColor,
                           UIColor.init(hexString: yellow, alpha: 0.5 ).cgColor,
                           UIColor.init(hexString: darkYellow, alpha: 0.5 ).cgColor]
        gradient.locations = [0.0 ,0.32 ,0.4, 0.98, 1.0]
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: saveButton.frame.size.width, height: saveButton.frame.size.height)
        gradient.cornerRadius = 4.5
        saveButton.layer.insertSublayer(gradient, at: 0)
        saveButton.setTitleColor(UIColor.white, for: .normal)
    }
    
    func addGestureRecognizer(_ uiImageView: UIImageView){
        let GestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        uiImageView.isUserInteractionEnabled = true
        uiImageView.addGestureRecognizer(GestureRecognizer)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        if (tapGestureRecognizer.view as? UIImageView) != nil {
            if let ViewController = UIStoryboard.init(name: "MapPickupLocationStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "MapPickupLocationViewController") as? MapPickupLocationViewController {
                self.navigationController?.pushViewController(ViewController, animated: true)
            }
        }
    }
    
    @IBAction func startButton(_ sender: Any) {
        
    }
    
    @IBAction func descriptionButtonPressed(_ sender: Any) {
        setupDescriptionView()
        descriptionTextView.becomeFirstResponder()
        blackView.isHidden = false
        descriptionViewIsActive = true
        tagsView.isHidden = true
        descriptionView.isHidden = false
    }
    
    @IBAction func tagButtonPressed(_ sender: Any) {
        blackView.isHidden = false
        tagsView.isHidden = false
        descriptionView.isHidden = true
        tagsViewIsActive = true
        tagsViewBottomConstraint.constant = 0
        self.view.layoutIfNeeded()
        tagsTableViewHeight.constant = tagsTableView.contentSize.height + 15
    }
    
    @IBAction func addFriendsButtonPressed(_ sender: Any) {
        contactTableView.reloadData()
        blackView.isHidden = false
        contactsView.isHidden = false
        contactsViewIsActive = true
        contactViewBottomConstraint.constant = 0
        self.view.layoutIfNeeded()
//        contactsTableViewHeight.constant = contactTableView.contentSize.height > UIScreen.main.bounds.height - 120 ?   UIScreen.main.bounds.height - 120 : contactTableView.contentSize.height + 15
    }
    
    @IBAction func addSelectedContacts(_ sender: Any) {
        friendsSelected = true
        imag1.isHidden = false
        img2.isHidden = false
        img3.isHidden = false
        remainingNumber.isHidden = true
        hideDescriptionView()
        checkTripButtons()
    }
    
    func checkTripButtons() {
        if tagSelected == true && friendsSelected == true && descriptionIsWritten == true {
            let gradient: CAGradientLayer = CAGradientLayer()
            gradient.startPoint = CGPoint(x: 1.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.colors = [UIColor.init(hexString: darkGreen, alpha: 1.0).cgColor,
                               UIColor.init(hexString: darkGreen, alpha: 1.0).cgColor,
                               UIColor.init(hexString: green, alpha: 1.0).cgColor,
                               UIColor.init(hexString: green, alpha: 1.0).cgColor,
                               UIColor.init(hexString: yellow, alpha: 0.85 ).cgColor]
            gradient.locations = [0.0 ,0.15, 0.4, 0.6, 1.0]
            gradient.frame = CGRect(x: 0.0, y: 0.0, width: startButton.frame.size.width, height: startButton.frame.size.height)
            gradient.cornerRadius = 4.5
            startButton.layer.insertSublayer(gradient, at: 0)
            startButton.setTitleColor(UIColor.white, for: .normal)
            
            addToUpComingOutings.layer.borderColor = UIColor.init(hexString: darkBlue, alpha: 1.0).cgColor
            addToUpComingOutings.layer.borderWidth = 0.5
            addToUpComingOutings.setTitleColor(UIColor.init(hexString: darkBlue, alpha: 1.0), for: .normal)
        }
    }
    
}

extension CreateOutingViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let rangeOfTextToReplace = Range(range, in: textView.text) else {
            return false
        }
        let substringToReplace = textView.text[rangeOfTextToReplace]
        let count = textView.text.count - substringToReplace.count + text.count
        numberLabel.text = "\(count)/200"
        return count <= 199
    }
}


extension CreateOutingViewController: onGetTagsPresenterToViewProtocol, onCreateOutingPresenterToViewProtocol{
    func onSuccessPhoneExist(response: PhoneExistResponse) { }
    func onFailuarPhoneExist(errorMessage: String) { }
    func showConnectionErro() { }
    
    func onSuccessCreateOuting(response: MainResponse) {
        
    }
    
    func onFailuarCreateOuting(errorMessage: String) {
        
    }
    
    func onConnectionErrorCreateOuting() {
        
    }
    
    func onSuccessGetTags(response: TagsResponse) {
        DispatchQueue.main.async {
            if let res = response.data {
                self.tagsDataSource = res
                self.tagsTableView.reloadData()
            }
            
        }
    }
    
    
    func onFailuarGetTags(errorMessage: String) {
        debugPrint(errorMessage)
    }
    
    func onConnectionErrorGetTags() {
        
    }
    
    
}


//extension SelectContactsDelegate {
//
//}
