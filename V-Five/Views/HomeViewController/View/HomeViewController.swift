//
//  HomeViewController.swift
//  V-Five
//
//  Created by Ahmed on 4/20/20.
//  Copyright © 2020 V-Five. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
//    @IBOutlet weak var requestedTableView: UITableView!
//    @IBOutlet weak var requestedTableViewHeight: NSLayoutConstraint!
//
//    @IBOutlet weak var upComingTableView: UITableView!
//    @IBOutlet weak var upComingTableViewHeight: NSLayoutConstraint!
//
//    @IBOutlet weak var norequestedLabel: UILabel!
    
    
    @IBOutlet weak var requestedTableView: UITableView!
    @IBOutlet weak var requestedTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var upComingTableView: UITableView!
    @IBOutlet weak var upComingTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var norequestedLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.norequestedLabel.text = ""
        requestedTableView.estimatedRowHeight =  50
        requestedTableView.rowHeight = UITableView.automaticDimension
        
        upComingTableView.estimatedRowHeight =  50
        upComingTableView.rowHeight = UITableView.automaticDimension
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 15) {
            self.requestedTableViewHeight.constant = 0.0
            self.norequestedLabel.text = "You don't have any requested trips yet."
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.layoutIfNeeded()
        requestedTableViewHeight.constant = requestedTableView.contentSize.height
        upComingTableViewHeight.constant = upComingTableView.contentSize.height
    }
    
}


extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == requestedTableView {
            return 2
        }
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == requestedTableView {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "RequestedCell", for: indexPath) as? RequestedTripTableViewCell {
                cell.selectionStyle = .none
                
//                            cell.configureCell(indexPath: indexPath)
                return cell
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "UpCommingTripCell", for: indexPath) as? UpCommingTripTableViewCell {
                cell.selectionStyle = .none
                
                //            cell.configureCell(indexPath: indexPath)
                return cell
            }
        }
        return UITableViewCell()
    }
}
