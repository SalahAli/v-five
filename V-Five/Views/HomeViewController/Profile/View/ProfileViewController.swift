//
//  ProfileViewController.swift
//  V-Five
//
//  Created by test on 15/05/2020.
//  Copyright © 2020 V-Five. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var profilePhoto: UIImageView!
    @IBOutlet weak var backIcon: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var termsArrow: UIImageView!
    @IBOutlet weak var languageArrow: UIImageView!
    @IBOutlet weak var appModeSwitch: UISwitch!
    @IBOutlet weak var logOutButton: UIButton!
    
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var languageTitle: UILabel!
    @IBOutlet weak var darkModeLabel: UILabel!
    @IBOutlet weak var termsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        UserProfilePresenter.shared.userProfileDelegate = self
//        UserProfilePresenter.shared.viewDidLoadIsFinished()
//        UserProfilePresenter.shared.getUserProfile()
        UISetupColorMode()
    }

    func UISetupColorMode() {
        topView.backgroundColor = UIColor.init(hexString: AppModeColor.getWhiteBackgroundColor())
        backIcon.setImage(UIImage(named: AppModeIcons.getbackProfileIcon()), for: .normal)
        editButton.setImage(UIImage(named: AppModeIcons.geteditProfileIcon()), for: .normal)
        termsArrow.image = UIImage(named: AppModeIcons.getArrowProfileIcon())
        languageArrow.image = UIImage(named: AppModeIcons.getArrowProfileIcon())
        self.view.backgroundColor = UIColor.init(hexString: AppModeColor.getBackgroundColor())
        userName.textColor = UIColor.init(hexString: AppModeColor.getWhiteTextColor())
        languageTitle.textColor = UIColor.init(hexString: AppModeColor.getWhiteTextColor())
        darkModeLabel.textColor = UIColor.init(hexString: AppModeColor.getWhiteTextColor())
        termsLabel.textColor = UIColor.init(hexString: AppModeColor.getWhiteTextColor())
        seperatorView.backgroundColor = UIColor.init(hexString: AppModeColor.getSeperatorColor())
    }
    
    override func viewWillLayoutSubviews() {
//        topView.roundCorners(corners: [.layerMaxXMaxYCorner, .layerMinXMaxYCorner],
//                             radius: 20.0,
//                             borderWidth: 0.4,
//                             borderColor: UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1.0),
//                             shadowColor: UIColor.lightGray,
//                             shadowOffsetWidth: 0,
//                             shadowOffsetHeight: -5,
//                             shadowOpicty: 0.5,
//                             shadowRadius: 41.5)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    @IBAction func logoutPressed(_ sender: UIButton) {
        
    }
    
    @IBAction func appModeSwitch(_ sender: UISwitch) {
        if sender.isOn {
            UserInfo.setCurrentAppMode(mode: "Dark")
            UISetupColorMode()
        } else {
            UserInfo.setCurrentAppMode(mode: "Light")
            UISetupColorMode()
        }
    }
    
    func updateViewWith(data: UserProfile) {
        
        phoneNumber.text = data.phone ?? ""
        userName.text = data.name ?? ""
        
        if let img = data.profileImage {
            profilePhoto.imageFromURL(urlString: img)
        }
    }
    
}

extension ProfileViewController: onUserProfilePresenterToViewProtocol {
    func onSuccessUserProfile(res: UserProfileResponse) {
        DispatchQueue.main.async {
            if res.status == 200 {
                if let profile = res.data {
                    self.updateViewWith(data: profile)
                }
            }
        }
    }
    
    func onFailuarUserProfile(errorMessage: String) {
        
    }
    
    func showConnectionErro() {
        
    }
    
    
}
