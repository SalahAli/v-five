//
//  UserProfileResponse.swift
//  V-Five
//
//  Created by Ahmed on 5/31/20.
//  Copyright © 2020 V-Five. All rights reserved.
//

import Foundation

struct UserProfileResponse : Codable {
    
    let data : UserProfile?
    let message : String?
    let status : Int?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case message = "message"
        case status = "status"
    }
}

struct UserProfile: Codable {
    
    let fcmToken : String?
    let phone : String?
    let phoneVerified : Bool?
    let profileImage : String?
    let name: String?
    
    
    enum CodingKeys: String, CodingKey {
        case fcmToken = "fcmToken"
        case phone = "phoneValue"
        case phoneVerified = "phoneVerified"
        case profileImage = "profileImage"
        case name = "username"
    }
}
