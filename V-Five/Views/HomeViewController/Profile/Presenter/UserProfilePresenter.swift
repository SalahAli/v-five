//
//  UserProfilePresenter.swift
//  V-Five
//
//  Created by Ahmed on 6/1/20.
//  Copyright © 2020 V-Five. All rights reserved.
//

import Foundation

class UserProfilePresenter: onUserProfileModelToPresenterProtocol {
   
    static let shared = UserProfilePresenter()
    var userProfileDelegate: onUserProfilePresenterToViewProtocol!
    
    func onSuccessUserProfile(response: UserProfileResponse) {
        userProfileDelegate.onSuccessUserProfile(res: response)
    }
    
    func onFailuarUserProfile(errorMessage: String) {
        userProfileDelegate.onFailuarUserProfile(errorMessage: errorMessage)
    }
    
    func onConnectionError() {
        userProfileDelegate.showConnectionErro()
    }
    
    func viewDidLoadIsFinished() {
        UserProfileModel.shared.userProfileDelegate = self
    }
    
    func getUserProfile() {
        UserProfileModel.shared.userProfileAPI()
    }
    
}

protocol onUserProfilePresenterToViewProtocol {
    func onSuccessUserProfile(res: UserProfileResponse)
    func onFailuarUserProfile(errorMessage:String)
    func showConnectionErro()
}

