//
//  UserProfileModel.swift
//  V-Five
//
//  Created by Ahmed on 6/1/20.
//  Copyright © 2020 V-Five. All rights reserved.
//

import Foundation

class UserProfileModel {
    static let shared = UserProfileModel()
    var userProfileDelegate: onUserProfileModelToPresenterProtocol!
    
    func userProfileAPI() {
        RequestManager.defaultManager.WebServiceApiRequest(service: Endpoints.userProfile(apiVersion: 1), parameters: nil) { (respone: UserProfileResponse?, error: Bool) in
            if !error {
                if let res = respone, res.status == 200 {
                    self.userProfileDelegate?.onSuccessUserProfile(response: res)
                } else {
                    self.userProfileDelegate?.onFailuarUserProfile(errorMessage: "user profile error")
                }
            } else {
                if !ReachabilityChecker.shared.isConnectedToNetwork() {
                    self.userProfileDelegate?.onConnectionError()
                }
            }
        }
    }
    
   
}


protocol onUserProfileModelToPresenterProtocol {
   func onSuccessUserProfile(response: UserProfileResponse)
   func onFailuarUserProfile(errorMessage: String)
   func onConnectionError()
}
