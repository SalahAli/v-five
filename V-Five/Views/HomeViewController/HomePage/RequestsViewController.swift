//
//  RequestsViewController.swift
//  V-Five
//
//  Created by test on 13/05/2020.
//  Copyright © 2020 V-Five. All rights reserved.
//

import UIKit
import ImageSlideshow

class RequestsViewController: UIViewController {

    @IBOutlet weak var myOutingsTableView: UITableView!
    @IBOutlet weak var requestsTableView: UITableView!
    @IBOutlet weak var requestsButton: UIButton!
    @IBOutlet weak var myOutingsButton: UIButton!
    let gradient: CAGradientLayer = CAGradientLayer()
    @IBOutlet weak var blackView: UIView!
    @IBOutlet weak var slideImagesShow: ImageSlideshow!
    @IBOutlet weak var imageIndicatorStackView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        requestsTableView.estimatedRowHeight =  50
        requestsTableView.rowHeight = UITableView.automaticDimension
        addGraidentColorTo(button: requestsButton)
        addGrayColorTo(button: myOutingsButton)
        requestsTableView.isHidden = false
        myOutingsTableView.isHidden = true
        blackView.isHidden = true
        slideImagesShow.isHidden = true
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
        slideImagesShow.addGestureRecognizer(gestureRecognizer)
        slideShowConfiguration()
        setupImagesStackView()
        imageIndicatorStackView.isHidden = true
    }
    
    @objc func didTap() {
//        slideImagesShow.presentFullScreenController(from: self)
        slideImagesShow.nextPage(animated: true)
    }
    
    func slideShowConfiguration()
    {
        slideImagesShow.setImageInputs([
            ImageSource(image: UIImage(named: "trip1")!),
            ImageSource(image: UIImage(named: "trip2")!),
            ImageSource(image: UIImage(named: "trip3")!),
            ImageSource(image: UIImage(named: "trip4")!)
        ])
        
        slideImagesShow.circular = true
        slideImagesShow.activityIndicator = DefaultActivityIndicator()
        slideImagesShow.activityIndicator = DefaultActivityIndicator(style: .medium, color: nil)
        slideImagesShow.contentScaleMode = .scaleAspectFill
        let pageIndicator = UIPageControl()
        pageIndicator.currentPageIndicatorTintColor = UIColor.clear
        pageIndicator.pageIndicatorTintColor = UIColor.clear
        slideImagesShow.pageIndicator = pageIndicator
    }
    
    var currentImageIndex = 0
    
    func setupImagesStackView() {
        let view1 = UIView()
        view1.layer.cornerRadius = 2
        view1.backgroundColor = .white
        
        let view2 = UIView()
        view2.layer.cornerRadius = 2
        view2.backgroundColor = UIColor.init(white: 1, alpha: 0.6)
        
        let view3 = UIView()
        view3.layer.cornerRadius = 2
        view3.backgroundColor = UIColor.init(white: 1, alpha: 0.6)
        
        let view4 = UIView()
        view4.layer.cornerRadius = 2
        view4.backgroundColor = UIColor.init(white: 1, alpha: 0.6)
           
           imageIndicatorStackView.addArrangedSubview(view1)
           imageIndicatorStackView.addArrangedSubview(view2)
           imageIndicatorStackView.addArrangedSubview(view3)
           imageIndicatorStackView.addArrangedSubview(view4)
           
           
           
           slideImagesShow.currentPageChanged = { page in
                // do whatever you want eg:
            self.currentImageIndex = page
               for (index,view) in self.imageIndicatorStackView.arrangedSubviews.enumerated() {
                   if index == page {
                       view.backgroundColor = .white
                   } else {
                       view.backgroundColor = UIColor.init(white: 1, alpha: 0.6)
                   }
               }
            
           }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            Timer.scheduledTimer(withTimeInterval: 3, repeats: true) { timer in
                for (index,view) in self.imageIndicatorStackView.arrangedSubviews.enumerated() {
                    if index == self.currentImageIndex {
                        view.backgroundColor = .white
                    } else {
                        view.backgroundColor = UIColor.init(white: 1, alpha: 0.6)
                    }
                }
                self.slideImagesShow.nextPage(animated: true)
            }
        }
       }
    
    func addGraidentColorTo(button: UIButton) {
        gradient.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.colors = [UIColor.init(hexString: darkGreen, alpha: 1.0).cgColor,
                           UIColor.init(hexString: green, alpha: 1.0).cgColor,
                           UIColor.init(hexString: green, alpha: 1.0).cgColor,
                           UIColor.init(hexString: darkYellow, alpha: 1.0 ).cgColor,
                           UIColor.init(hexString: yellow, alpha: 1.0 ).cgColor]
        gradient.locations = [0.0 ,0.3 ,0.4, 0.85, 1.0]
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: button.frame.size.width, height: button.frame.size.height)
        gradient.cornerRadius = 4.5
        button.layer.insertSublayer(gradient, at: 0)
        button.setTitleColor(UIColor.white, for: .normal)
    }
    
    func  addGrayColorTo(button: UIButton) {
        button.setTitleColor(UIColor.init(hexString: darkBlack), for: .normal)
        button.backgroundColor = UIColor.init(hexString: grayButton)
    }
    
    @IBAction func requestsButtonPressed(_ sender: Any) {
        gradient.removeFromSuperlayer()
        addGrayColorTo(button: myOutingsButton)
        addGraidentColorTo(button: requestsButton)
        
        requestsTableView.isHidden = false
        myOutingsTableView.isHidden = true
    }
    
    @IBAction func myTripsButtonPressed(_ sender: Any) {
        gradient.removeFromSuperlayer()
        addGrayColorTo(button: requestsButton)
        addGraidentColorTo(button: myOutingsButton)
        requestsTableView.isHidden = true
        myOutingsTableView.isHidden = false
    }
    
    @IBAction func slideshowImagesButton(_ sender: Any) {
        blackView.isHidden = false
        slideImagesShow.isHidden = false
        imageIndicatorStackView.isHidden = false
    }
    
    @IBAction func dismissButton(_ sender: Any) {
        blackView.isHidden = true
        slideImagesShow.isHidden = true
        imageIndicatorStackView.isHidden = true
    }
}

extension RequestsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == requestsTableView {
            if section == 0 {
                return 1 //requestedTripsList.count
            } else {
                return 3 //UpComingTripsList.count
            }
        } else {
            return 6
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == requestsTableView {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == requestsTableView {
            if indexPath.section == 0 {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "RequestOutingCell", for: indexPath) as? RequestOutingTableViewCell {
                    cell.selectionStyle = .none
                    return cell
                }
            } else {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "UpComingCell", for: indexPath) as? UpComingTableViewCell {
                    cell.selectionStyle = .none
                    return cell
                }
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "MyOutingCell", for: indexPath) as? MyOutingTableViewCell {
                cell.selectionStyle = .none
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == requestsTableView {
            return 33
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == requestsTableView {
            let headerView = UIView()
            headerView.backgroundColor = UIColor.clear
            let sectionTitle = UILabel()
            sectionTitle.text = section == 0 ? "Requested Trips" : "Up ComingTrips"
            sectionTitle.frame = CGRect(x: 15, y: 5, width: tableView.frame.width, height: 30)
            sectionTitle.font = UIFont(name: "OpenSans-Bold", size: 17)
            sectionTitle.textColor = UIColor.init(hexString: darkBlack)
            headerView.addSubview(sectionTitle)
            return headerView
        }
        return UIView()
    }
}
