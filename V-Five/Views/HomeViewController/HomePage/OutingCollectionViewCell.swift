//
//  CollectionViewCell.swift
//  V-Five
//
//  Created by test on 23/04/2020.
//  Copyright © 2020 V-Five. All rights reserved.
//

import UIKit

class OutingCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var outingImage: UIImageView!
    
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    @IBOutlet weak var remainingNumber: UILabel!
    
    @IBOutlet weak var reactsNumber: UILabel!
    @IBOutlet weak var reactButton: UIButton!
    
    @IBOutlet weak var tagView: UIView!
    @IBOutlet weak var tagLabel: UILabel!
    
    var reactsNumberList: [String] = ["150", "320", "20", "187", "430", "560", "268", "750"]
    var outingImages: [String] = ["outing1","outing2","outing3","ali","outing5","outing6","outing1","outing2"]
    
    func configureCell(indexPath: Int) {
        reactsNumber.text = reactsNumberList[indexPath]
        outingImage.image = UIImage(named: outingImages[indexPath])
        if indexPath == 2 {
            remainingNumber.isHidden = false
            remainingNumber.text = "6"
        } else if indexPath == 5 {
            remainingNumber.isHidden = false
            remainingNumber.text = "+9"
        } else {
            remainingNumber.isHidden = true
        }
        
        if indexPath == 0 || indexPath == 1 {
            image3.isHidden = true
            image2.isHidden = false
            image1.isHidden = false
            remainingNumber.isHidden = true
        } else if indexPath == 3 || indexPath == 4 {
            image3.isHidden = true
            image2.isHidden = true
            image1.isHidden = false
            remainingNumber.isHidden = true
        } else {
            image3.isHidden = false
            image2.isHidden = false
            image1.isHidden = false
            remainingNumber.text = "4"
            remainingNumber.isHidden = false
        }
        
        if indexPath == 0 {
            tagView.backgroundColor = UIColor.init(hexString: orangeTagColor, alpha: 0.1)
            tagLabel.textColor =  UIColor.init(hexString: orangeTagColor, alpha: 1.0)
        } else if indexPath == 1 {
            tagView.backgroundColor = UIColor.init(hexString: PurpleTagColor, alpha: 0.1)
            tagLabel.textColor =  UIColor.init(hexString: PurpleTagColor, alpha: 1.0)
            tagLabel.text = "Library"
        } else if indexPath == 2 {
            tagView.backgroundColor = UIColor.init(hexString: blueTagColor, alpha: 0.1)
            tagLabel.textColor =  UIColor.init(hexString: blueTagColor, alpha: 1.0)
            tagLabel.text = "Cafe"
        } else if indexPath == 3 {
            tagView.backgroundColor = UIColor.init(hexString: greenTagColor, alpha: 0.1)
            tagLabel.textColor =  UIColor.init(hexString: greenTagColor, alpha: 1.0)
            tagLabel.text = "Restaurant"
        } else if indexPath == 4 {
            tagView.backgroundColor = UIColor.init(hexString: pinkTagColor, alpha: 0.1)
            tagLabel.textColor =  UIColor.init(hexString: pinkTagColor, alpha: 1.0)
            tagLabel.text = "Mall"
        }
        else if indexPath == 5 {
            tagView.backgroundColor = UIColor.init(hexString: turquoiseTagColor, alpha: 0.1)
            tagLabel.textColor =  UIColor.init(hexString: turquoiseTagColor, alpha: 1.0)
            tagLabel.text = "Park"
        }
        else if indexPath == 6 {
            tagView.backgroundColor = UIColor.init(hexString: redTagColor, alpha: 0.1)
            tagLabel.textColor =  UIColor.init(hexString: redTagColor, alpha: 1.0)
            tagLabel.text = "Event"
        }
        else if indexPath == 7 {
            tagView.backgroundColor = UIColor.init(hexString: orangeTagColor, alpha: 0.1)
            tagLabel.textColor =  UIColor.init(hexString: orangeTagColor, alpha: 1.0)
        }
        
    }
}
