//
//  ParentTabBarViewController.swift
//  V-Five
//
//  Created by test on 13/05/2020.
//  Copyright © 2020 V-Five. All rights reserved.
//

import UIKit

class ParentTabBarViewController: UIViewController {

    @IBOutlet weak var entryScreen: UIView!
    @IBOutlet weak var outingScreen: UIView!
    
    @IBOutlet weak var blackViewWidth: NSLayoutConstraint!
    @IBOutlet weak var blackViewLeading: NSLayoutConstraint!
    @IBOutlet weak var outingLabel: UILabel!
    @IBOutlet weak var homeLabel: UILabel!
    @IBOutlet weak var homeIcon: UIImageView!
    @IBOutlet weak var outingIcon: UIImageView!
    @IBOutlet weak var outingViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var tabBarView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.blackViewWidth.constant = "Home".size(withAttributes:[.font: UIFont.systemFont(ofSize: 16)]).width + 60.0
        self.blackViewLeading.constant = 14
        self.outingViewLeadingConstraint.constant = 14 + "Home".size(withAttributes:[.font: UIFont.systemFont(ofSize: 16)]).width + 60.0 + 16
        self.homeIcon.image = UIImage(named: "homeActive")
        self.outingIcon.image = UIImage(named: "markerNotActive")
        self.homeLabel.isHidden = false
        self.outingLabel.isHidden = true
        entryScreen.alpha = 1.0
        outingScreen.alpha = 0.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func profileButtonPressed(_ sender: Any) {
        if let ViewController = UIStoryboard.init(name: "ProfileStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProfileView") as? ProfileViewController {
            self.navigationController?.pushViewController(ViewController, animated: true)
        }
    }
    
    override func viewWillLayoutSubviews() {
        tabBarView.roundCorners(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner],
                                radius: 20.0,
                                borderWidth: 0.4,
                                borderColor: UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1.0),
                                shadowColor: UIColor(red: 193/255, green: 193/255, blue: 193/255, alpha: 0.8),
                                shadowOffsetWidth: 0,
                                shadowOffsetHeight: -5,
                                shadowOpicty: 0.5,
                                shadowRadius: 5)
    }
    
    
    @IBAction func homeButtonPressed(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.blackViewWidth.constant = "Home".size(withAttributes:[.font: UIFont.systemFont(ofSize: 16)]).width + 60.0
            self.blackViewLeading.constant = 14
            self.outingViewLeadingConstraint.constant = 14 + "Home".size(withAttributes:[.font: UIFont.systemFont(ofSize: 16)]).width + 60.0 + 16
            self.homeIcon.image = UIImage(named: "homeActive")
            self.outingIcon.image = UIImage(named: "markerNotActive")
            self.homeLabel.isHidden = false
            self.outingLabel.isHidden = true
            self.view.layoutIfNeeded()
        })
        
        entryScreen.alpha = 1.0
        outingScreen.alpha = 0.0
    }
    
    @IBAction func outingsButtonPressed(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.blackViewWidth.constant = "Outing".size(withAttributes:[.font: UIFont.systemFont(ofSize: 16)]).width + 60
            self.blackViewLeading.constant = 85.0 //14 + "Home".size(withAttributes:[.font: UIFont.systemFont(ofSize: 16)]).width + 60.0 + 16 + 5
            self.outingViewLeadingConstraint.constant = 80 //14 + "Home".size(withAttributes:[.font: UIFont.systemFont(ofSize: 16)]).width + 60.0 + 16
            self.homeIcon.image = UIImage(named: "homeNotActive")
            self.outingIcon.image = UIImage(named: "markerActive")
            
            self.homeLabel.isHidden = true
            self.outingLabel.isHidden = false
            self.view.layoutIfNeeded()
        })
        
        entryScreen.alpha = 0.0
        outingScreen.alpha = 1.0
    }
    
}
