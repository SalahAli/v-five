//
//  EnteryViewController.swift
//  V-Five
//
//  Created by test on 03/05/2020.
//  Copyright © 2020 V-Five. All rights reserved.
//

import UIKit
import ImageSlideshow

class EnteryViewController: UIViewController {

    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var blackViewButton: UIButton!
    @IBOutlet weak var outingViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var outingImage: UIImageView!
    @IBOutlet weak var outingBody: UILabel!
    @IBOutlet weak var currentTripOnMapView: UIView!
    
    @IBOutlet weak var imagesIndicatorStackView: UIStackView!
    @IBOutlet weak var slideshow: ImageSlideshow!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addGradientLayer()
        self.outingViewTopConstraint.constant =  UIScreen.main.bounds.height
        let tapGesture = UITapGestureRecognizer(target: self,
        action: #selector(openMapViewController))
        currentTripOnMapView.addGestureRecognizer(tapGesture)
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
        slideshow.addGestureRecognizer(gestureRecognizer)
        slideShowConfiguration()
        setupImagesStackView()
        imagesIndicatorStackView.isHidden = true
    }
    
    @objc func openMapViewController() {
        if currentTripOnMapView != nil {
            if let ViewController = UIStoryboard.init(name: "PassengersListStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "PassengersOnMapViewController") as? PassengersOnMapViewController {
                self.navigationController?.pushViewController(ViewController, animated: true)
            }
        }
    }
    
    @objc func didTap() {
//        slideshow.presentFullScreenController(from: self)
        slideshow.nextPage(animated: true)
    }

    func slideShowConfiguration()
    {
        slideshow.setImageInputs([
            ImageSource(image: UIImage(named: "shot2")!),
            ImageSource(image: UIImage(named: "shot1")!),
            ImageSource(image: UIImage(named: "shot3")!),
            ImageSource(image: UIImage(named: "shot4")!)
        ])
        slideshow.circular = true
        slideshow.activityIndicator = DefaultActivityIndicator()
        slideshow.activityIndicator = DefaultActivityIndicator(style: .medium, color: nil)
        slideshow.contentScaleMode = .scaleAspectFill
        let pageIndicator = UIPageControl()
        pageIndicator.currentPageIndicatorTintColor = UIColor.clear
        pageIndicator.pageIndicatorTintColor = UIColor.clear
        slideshow.pageIndicator = pageIndicator
        
    }
    
    var currentImageIndex = 0
    
    func setupImagesStackView() {
     let view1 = UIView()
     view1.layer.cornerRadius = 2
     view1.backgroundColor = .white
     
     let view2 = UIView()
     view2.layer.cornerRadius = 2
     view2.backgroundColor = UIColor.init(white: 1, alpha: 0.6)
     
     let view3 = UIView()
     view3.layer.cornerRadius = 2
     view3.backgroundColor = UIColor.init(white: 1, alpha: 0.6)
     
     let view4 = UIView()
     view4.layer.cornerRadius = 2
     view4.backgroundColor = UIColor.init(white: 1, alpha: 0.6)
        
        imagesIndicatorStackView.addArrangedSubview(view1)
        imagesIndicatorStackView.addArrangedSubview(view2)
        imagesIndicatorStackView.addArrangedSubview(view3)
        imagesIndicatorStackView.addArrangedSubview(view4)
        
        
        
        slideshow.currentPageChanged = { page in
             // do whatever you want eg:
         self.currentImageIndex = page
            for (index,view) in self.imagesIndicatorStackView.arrangedSubviews.enumerated() {
                if index == page {
                    view.backgroundColor = .white
                } else {
                    view.backgroundColor = UIColor.init(white: 1, alpha: 0.6)
                }
            }
         
            self.imagesIndicatorStackView.isHidden = false
        }
     
     DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
         Timer.scheduledTimer(withTimeInterval: 3, repeats: true) { timer in
             for (index,view) in self.imagesIndicatorStackView.arrangedSubviews.enumerated() {
                 if index == self.currentImageIndex {
                     view.backgroundColor = .white
                 } else {
                     view.backgroundColor = UIColor.init(white: 1, alpha: 0.6)
                 }
             }
             self.slideshow.nextPage(animated: true)
         }
     }
    }
    
    func addGradientLayer() {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.colors = [UIColor.init(hexString: darkBlue, alpha: 1.0).cgColor,
                           UIColor.init(hexString: green, alpha: 1.0).cgColor,
                           UIColor.init(hexString: yellow, alpha: 1.0 ).cgColor,
                           UIColor.init(hexString: yellow, alpha: 1.0 ).cgColor]
        gradient.locations = [0.05 ,0.4, 0.9, 1.0]
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.addView.frame.size.width, height: self.addView.frame.size.height)
        gradient.cornerRadius = 12.9
        self.addView.layer.insertSublayer(gradient, at: 0)
        
    }

    @IBAction func blackViewButtonPressed(_ sender: Any) {
        blackViewButton.isHidden = true
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.outingViewTopConstraint.constant = UIScreen.main.bounds.height
            self.outingImage.alpha = 0.0
            self.outingBody.alpha = 0.0
            self.view.layoutIfNeeded()
        })
        
        imagesIndicatorStackView.isHidden = true
    }
//
//    @IBAction func addOutingButtonPressed(_ sender: Any) {
//        if let ViewController = UIStoryboard.init(name: "CreateOutingStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateOutingView") as? CreateOutingViewController {
//            self.navigationController?.pushViewController(ViewController, animated: true)
//        }
//    }
    
    @IBAction func addNewOutingPressed(_ sender: UIButton) {
        if let ViewController = UIStoryboard.init(name: "CreateOutingStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateOutingView") as? CreateOutingViewController {
            self.navigationController?.pushViewController(ViewController, animated: true)
        }
    }
    
    
    
    
    
}

extension EnteryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OutingCell", for: indexPath) as? OutingCollectionViewCell {
            cell.imageHeightConstraint.constant = ((collectionView.frame.width / 2) - 7.5) + 10
            cell.imageWidthConstraint.constant = (collectionView.frame.width / 2) - 7.5
            cell.configureCell(indexPath: indexPath.row)
            return cell
        }
        
        return UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.outingImage.alpha = 1.0
            self.outingBody.alpha = 1.0
            self.outingViewTopConstraint.constant =  80
            self.view.layoutIfNeeded()
        })
        self.blackViewButton.isHidden = false
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width / 2) - 7.5, height: ((collectionView.frame.width / 2) - 7.5) * 1.24 )
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
            return CGSize(width: collectionView.frame.width, height: 0)
    }
}
