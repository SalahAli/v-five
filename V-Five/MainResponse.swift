//
//  MainResponse.swift
//  V-Five
//
//  Created by Ahmed on 6/23/20.
//  Copyright © 2020 V-Five. All rights reserved.
//

import Foundation

struct MainResponse: Codable {
    let status: Int?
    let message: String?
}
