//
//  Language.swift
//  Naqelat
//
//  Created by Khaled saad on 11/30/17.
//  Copyright © 2017 Asgatech. All rights reserved.
//

import UIKit

let APPLE_LANGUAGE_KEY = "AppleLanguages"

class Language: NSObject {
    
    static var currentLanguage : AppLanguage {
        
        var langStr = "en"
        
        if let lang = Cache.object(key: "currentLang") as? String {
            langStr = lang
        } else {
            langStr = Locale.current.languageCode!
        }
        return langStr == "ar" ? .arabic : .english
    }
    static func setCurrentLanguage(lang : AppLanguage) {
        
        Cache.set(object: lang.rawValue, forKey: "currentLang")
        
        UserDefaults.standard.set([lang.rawValue], forKey: "AppleLanguages")
        UserDefaults.standard.synchronize()
        
        
        
    }
    static func swichLanguage() {
        
        switch Language.currentLanguage {
        case .arabic:
            self.setCurrentLanguage(lang: .english)
             AllUserDefaults.SETAPPLang("en")
            break
        case .english:
            self.setCurrentLanguage(lang: .arabic)
            AllUserDefaults.SETAPPLang("ar")
            break
        }
    }
    
    
    
    class var isRTL: Bool {
        return Language.currentLanguage == .arabic
    }
}
enum AppLanguage : String {
    case arabic = "ar"
    case english = "en"
}

class Lang: NSObject {
    
    static func localize() {
        MethodSwizzleGivenClassName(cls: Bundle.self, originalSelector: #selector(Bundle.localizedString(forKey:value:table:)), overrideSelector: #selector(Bundle.specialLocalizedStringForKey(_:value:table:)))
//        MethodSwizzleGivenClassName(cls: UITextField.self, originalSelector: #selector(UITextField.awakeFromNib), overrideSelector: #selector(UITextField.cstmAwakeFromNib))
//        MethodSwizzleGivenClassName(cls: UILabel.self, originalSelector: #selector(UILabel.awakeFromNib), overrideSelector: #selector(UILabel.cstmAwakeFromNib))
//        MethodSwizzleGivenClassName(cls: UIButton.self, originalSelector: #selector(UIButton.awakeFromNib), overrideSelector: #selector(UIButton.cstmAwakeFromNib))
//
//        MethodSwizzleGivenClassName(cls: UILabel.self, originalSelector: #selector(setter: UILabel.text), overrideSelector:#selector(UILabel.customSetText(text:)))
    }
}

func MethodSwizzleGivenClassName(cls: AnyClass, originalSelector: Selector, overrideSelector: Selector) {
    
    let origMethod: Method = class_getInstanceMethod(cls, originalSelector)!;
    let overrideMethod: Method = class_getInstanceMethod(cls, overrideSelector)!;
    if (class_addMethod(cls, originalSelector, method_getImplementation(overrideMethod), method_getTypeEncoding(overrideMethod))) {
        class_replaceMethod(cls, overrideSelector, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
    } else {
        method_exchangeImplementations(origMethod, overrideMethod);
    }
}

extension UIApplication {
    
    class func isRTL() -> Bool{
        return Language.currentLanguage == .arabic
    }
}
extension Bundle {
    @objc func specialLocalizedStringForKey(_ key: String, value: String?, table tableName: String?) -> String {
        if self == Bundle.main {
            let currentLanguage = Language.currentLanguage.rawValue
            var bundle = Bundle();
            
            if let _path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj") {
                bundle = Bundle(path: _path)!
            } else {
                let _path = Bundle.main.path(forResource: "Base", ofType: "lproj")!
                bundle = Bundle(path: _path)!
            }
            
            return (bundle.specialLocalizedStringForKey(key, value: value, table: tableName))
        } else {
            return (self.specialLocalizedStringForKey(key, value: value, table: tableName))
        }
    }
    var localizedMain : Bundle {
        
        get {
            if let path = Bundle.main.path(forResource: UIApplication.isRTL() ? "ar" : "en", ofType: "lproj") {
                
                if let bundle = Bundle.init(path: path) {
                    return bundle
                } else {
                    return Bundle.main
                }
                
            } else {
                return Bundle.main
            }
        }
        
    }
}

extension UITextField {
    @objc public func cstmAwakeFromNib() {
        self.cstmAwakeFromNib()
        
        let fontName = (self.font?.isBold)! ? ".HelveticaNeueDeskInterface-Bold" : ".HelveticaNeueDeskInterface-Regular"
        let font = UIFont.init(name: fontName, size: (self.font?.pointSize)!)
        self.font = font
        
        if self.textAlignment == .center { return }
        
        if UIApplication.isRTL()  {
            if self.textAlignment == .right { return }
            self.textAlignment = .right
        } else {
            if self.textAlignment == .left { return }
            self.textAlignment = .left
        }
    }
}
extension UIButton {
    @objc public func cstmAwakeFromNib() {
        self.cstmAwakeFromNib()
        
        let fontName = (self.titleLabel?.font?.isBold)! ? ".HelveticaNeueDeskInterface-Bold" : ".HelveticaNeueDeskInterface-Regular"
        let font = UIFont.init(name: fontName, size: (self.titleLabel?.font?.pointSize)!)
        self.titleLabel?.font = font
        
        
        if self.contentHorizontalAlignment == .center { return }
        
        if UIApplication.isRTL()  {
            if self.contentHorizontalAlignment == .right { return }
            self.contentHorizontalAlignment = .right
        } else {
            if self.contentHorizontalAlignment == .left { return }
            self.contentHorizontalAlignment = .left
        }
    }
}

extension UILabel {
    
//    @objc public func cstmAwakeFromNib() {
//        self.cstmAwakeFromNib()
//
//        let fontName = (self.font?.isBold)! ? ".Arial-Bold" : ".Arial-Regular"
//        let font = UIFont.init(name: fontName, size: (self.font?.pointSize)!)
//        self.font = font
//
//        self.text = self.text?.localized
//        self.adjustsFontSizeToFitWidth = true
//        if self.textAlignment == .center { return }
//
//        if UIApplication.isRTL()  {
//            if self.textAlignment == .right { return }
//            self.textAlignment = .right
//        } else {
//            if self.textAlignment == .left { return }
//            self.textAlignment = .left
//        }
//    }
//
//
//    @objc func customSetText(text: String) {
//        self.customSetText(text: text.localized)
//    }
}

extension UIFont {
    var isBold: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitBold)
    }
    
    var isItalic: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitItalic)
    }
}

@available(iOS 11.0, *)
extension UIApplication {
    
    class func initWindow(){
        
        (UIApplication.shared.delegate as! AppDelegate).initWindow()
    }
    
}
