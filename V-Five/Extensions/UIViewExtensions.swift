import Foundation
import UIKit
extension UIView {
    
    func customShadowAndRaduis(shadowOpicty: Float, corner: CGFloat, shadowOffsetWidth: Int, shadowOffsetHeight: Int, color: UIColor, shadowRadius: CGFloat){
        clipsToBounds = false
        layer.shadowOpacity = shadowOpicty
        layer.cornerRadius = corner
        layer.shadowRadius = shadowRadius
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight)
        layer.shadowColor = color.cgColor
        
    }
    
    func addShadowForView(shadowOpicty: Float, corner: CGFloat, shadowOffsetWidth: Int, shadowOffsetHeight: Int, color: UIColor) {
        clipsToBounds = false
        layer.shadowOpacity = shadowOpicty
        layer.shadowRadius = corner
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight)
        layer.shadowColor = color.cgColor
    }
    
//    func underlineMyText() {
//        guard let text = self.titleLabel?.text else { return }
//
//        let attributedString = NSMutableAttributedString(string: text)
//        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
//
//        self.setAttributedTitle(attributedString, for: .normal)
//    }

    func roundCorners(corners: CACornerMask, radius: CGFloat, borderWidth: CGFloat, borderColor: UIColor, shadowColor: UIColor, shadowOffsetWidth: Int, shadowOffsetHeight: Int, shadowOpicty: Float, shadowRadius: CGFloat) {
        clipsToBounds = false
        layer.cornerRadius = radius
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        layer.maskedCorners = corners
        
        layer.shadowOpacity = shadowOpicty
        layer.shadowRadius = shadowRadius
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight)
        layer.shadowColor = shadowColor.cgColor
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: radius)
        layer.shadowPath = shadowPath.cgPath
    }
    
    func bottomBoarder(color: CGColor, height: CGFloat){
        
        let borderName = CALayer()
        let widthName = CGFloat(2.0)
        
        borderName.borderColor = color
        borderName.frame = CGRect(x: 0, y: frame.size.height - widthName, width:  frame.width , height: height)
        borderName.borderWidth = widthName
        layer.addSublayer(borderName)
        layer.masksToBounds = true
        
    }
    
    
    func addDashedBorder(corner: CGFloat) {
        let color = UIColor.init(hexString: dashedBorder, alpha: 0.9).cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 0.5
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [4,4]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: corner).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
    
    func applyShadow(colorStr: String, alpha: CGFloat = 1) {
        layer.shadowOpacity = 0.5
        layer.cornerRadius = 4.0
        layer.shadowRadius = 4.0
        layer.shadowOffset = CGSize(width: 1, height: 3)
        layer.shadowColor = UIColor.init(hexString: colorStr, alpha: alpha).cgColor
        clipsToBounds = true
    }
    
    func applyShadow() {
        layer.shadowOpacity = 0.5
        layer.cornerRadius = 4.0
        layer.shadowRadius = 4.0
        layer.shadowOffset = CGSize(width: 1, height: 3)
        layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.50).cgColor
        clipsToBounds = false
    }
    
    func setGradient(colorTop: UIColor, colorBottom: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom.cgColor, colorTop.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = bounds
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func setGradient(colorRight: UIColor, colorLeft: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorLeft.cgColor, colorRight.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = bounds
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        
        self.layer.add(animation, forKey: nil)
    }
    
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
}
