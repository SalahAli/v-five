//
//  TextFieldExtensions.swift
//  Aoun
//
//  Created by Ahmed on 2/7/19.
//  Copyright © 2019 Ahmed Salah. All rights reserved.
//

import UIKit
extension UITextField {
    // add left or right image to TextField
    func textFieldContain(imageName:String, width: CGFloat, height: CGFloat){
        rightViewMode =  .always
        let imageView = UIImageView(frame: CGRect(x: 12, y: 0, width: width, height: height))
        let image = UIImageView(image: UIImage(named: imageName))
        imageView.image = image.image
        leftView = imageView

    }
    
//    func textFieldContain(image:UIImage, width: CGFloat = 38, height: CGFloat = 38){
//        rightViewMode =  .always
//        let imageView = UIImageView(frame: CGRect(x: 12, y: 0, width: width, height: height))
//        // to round image
//        imageView.layer.cornerRadius = imageView.frame.width / 2
//        imageView.layer.masksToBounds = true
//        //
//        let image = UIImageView(image: image)
//        imageView.image = image.image
//        rightView = imageView
//
//    }
    
    func borderRed() {
        borderColor = .red
        borderWidth = 1
        if let p = self.placeholder {
            attributedPlaceholder = NSAttributedString(string: p, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        }
    }
    
    func borderDefault() {
        borderColor = .lightText
        borderWidth = 1
        if let p = self.placeholder {
             attributedPlaceholder = NSAttributedString(string: p, attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightText])
        }
       
        
    }
    
}


