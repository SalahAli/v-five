//
//  UIViewControllerExtensions.swift
//  MetrVocato
//
//  Created by Ahmed Salah on 6/4/18.
//  Copyright © 2018 Islam. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func validateEmail(_ email:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    //  coustom allerts
    func alert(message: String){
        let alertView = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default) { (action) in
            // pop here
        }
        alertView.addAction(OKAction)
        present(alertView, animated: true, completion: nil)
    }
    
    func validateAlphabetical(_ text: String) -> Bool {
        let myCharSet=CharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
        let output: String = text.trimmingCharacters(in: myCharSet.inverted)
        let isValid: Bool = (text == output)
        print("\(isValid)")
        
        return isValid
}
    
    func setMainBackground() {
        //        view.backgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        //        view.contentMode = .scaleAspectFit
        //        view.clipsToBounds = true
        
        UIGraphicsBeginImageContext(view.frame.size)
        var image = UIImage(named: "background")
        image?.draw(in: view.bounds)
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        view.backgroundColor = UIColor(patternImage: image!)
    }
    
    func round(_ image: UIImageView){
        image.layer.cornerRadius = image.frame.width / 2
        image.layer.masksToBounds = true
    }
    
    func roundButton(_ button: UIButton){
        button.layer.cornerRadius = button.frame.width / 2
        button.layer.masksToBounds = true
    }
    

    
}
