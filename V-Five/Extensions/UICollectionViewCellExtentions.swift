//
//  UICollectionViewCellExtentions.swift
//  Traveller
//
//  Created by Ahmed on 6/8/18.
//  Copyright © 2018 Ahmed Salah. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
    func round(_ image: UIImageView){
        image.layer.cornerRadius = image.frame.width / 2
        image.layer.masksToBounds = true
    }
}
