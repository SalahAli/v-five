//
//  UITableViewCellExtensions.swift
//  EasyOrder
//
//  Created by Ahmed on 10/21/18.
//  Copyright © 2018 Ahmed Salah. All rights reserved.
//

import UIKit

extension UITableViewCell {
    func round(_ image: UIImageView){
        image.layer.cornerRadius = image.frame.width / 2
        image.layer.masksToBounds = true
    }
}
