//
//  UIColorExtensions.swift
//  MetrVocato
//
//  Created by Ahmed Salah on 5/31/18.
//  Copyright © 2018 Islam. All rights reserved.
//

import Foundation
import UIKit
extension UIColor {
    
    convenience init(hex:Int, alpha:CGFloat = 1.0) {
        self.init(
            red:   CGFloat((hex & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((hex & 0x00FF00) >> 8)  / 255.0,
            blue:  CGFloat((hex & 0x0000FF) >> 0)  / 255.0,
            alpha: alpha
        )
    }
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    //MARK :-
    // UIColor extensions for change color type
    @nonobjc static let TBTitle : UIColor = UIColor(red: 0.49, green: 0.37, blue: 0.15, alpha: 1.00)
    @nonobjc static let NBGold : UIColor = UIColor(red: 1.67, green: 1.36, blue: 0.66, alpha: 1.00)
    @nonobjc static let favBrowen : UIColor = UIColor(red: 0.80, green: 0.64, blue: 0.29, alpha: 1.00)
    @nonobjc static let borderGray : UIColor = UIColor(red: 225/255, green: 218/255, blue: 211/255, alpha: 1.00)
    @nonobjc static let fontGray : UIColor = UIColor(red: 0.43, green: 0.43, blue: 0.43, alpha: 1.00)
    @nonobjc static let grayPlaceholder : UIColor = UIColor(red: 127/255, green: 127/255, blue: 127/255, alpha: 1.00)
    

    
    convenience init(hex: Int) {
        let components = (
            R: CGFloat((hex >> 16) & 0xff) / 255,
            G: CGFloat((hex >> 08) & 0xff) / 255,
            B: CGFloat((hex >> 00) & 0xff) / 255
        )
        
        self.init(red: components.R, green: components.G, blue: components.B, alpha: 1)
    }
    //New Method to convert Hexa Color String
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
    
    
}
