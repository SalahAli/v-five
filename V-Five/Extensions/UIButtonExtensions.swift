//
//  UIButtonExtensions.swift
//  MetrVocato
//
//  Created by Ahmed Salah on 5/31/18.
//  Copyright © 2018 Islam. All rights reserved.
//

import Foundation
import UIKit
extension UIButton {
    
    func setbutton_image(image : String)  {
        self.tintColor = UIColor.white
        self.setImage(UIImage(named:image), for: .normal)
        self.imageEdgeInsets = UIEdgeInsets(top: 4,left: 4,bottom: 4,right: 60)
        
    }
    
    func setbutton_titel()  {
        self.titleEdgeInsets = UIEdgeInsets(top: 4,left:35,bottom: 4,right: 5)
    }
    func setbuttonsize() {
        self.titleLabel?.minimumScaleFactor = 0.5
        self.titleLabel?.numberOfLines = 0
        self.titleLabel?.adjustsFontSizeToFitWidth = true
    }
    
    //to add title with image inset button
    func selectedButton(title:String, iconName: String){
        //        self.backgroundColor = UIColor(red: 0, green: 118/255, blue: 254/255, alpha: 1)
        self.setTitle(title, for: .normal)
        self.setTitle(title, for: .highlighted)
        self.setTitleColor(UIColor.white, for: .normal)
        self.setTitleColor(UIColor.white, for: .highlighted)
        
        //        self.titleLabel?.adjustsFontSizeToFitWidth = true
        
        
        titleLabel?.numberOfLines = 1
        titleLabel?.adjustsFontSizeToFitWidth = true
        titleLabel?.minimumScaleFactor = 0.2
        //        titleLabel?.baselineAdjustment = .alignCenters
        //        titleLabel?.lineBreakMode = .
        
        self.setImage(UIImage(named: iconName), for: .normal)
        self.setImage(UIImage(named: iconName), for: .highlighted)
        let imageWidth = self.imageView!.frame.width
        let textWidth = (title as NSString).size(withAttributes:[NSAttributedString.Key.font:self.titleLabel!.font!]).width
        
        var width = CGFloat()
        width = textWidth + imageWidth + 12
        
        //24 - the sum of your insets from left and right
        //        widthConstraints.constant = width
        var imageInset = UIEdgeInsets()
        if Language.currentLanguage == .arabic {
            imageInset = UIEdgeInsets(top: 0, left: width, bottom: 0, right: 0)
        } else {
            imageInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: width)
            
        }
        
        self.imageEdgeInsets = imageInset
        
        self.layoutIfNeeded()
    }
    
  
    
}
